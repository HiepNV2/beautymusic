package com.example.beautymusic;

import android.app.Application;

import com.example.beautymusic.di.component.ApplicationComponent;
import com.example.beautymusic.di.component.DaggerApplicationComponent;
import com.example.beautymusic.di.module.ApplicationModule;

public class BeautyMusic extends Application {

    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();

        mApplicationComponent.inject(this);
    }

    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }

    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }
}
