package com.example.beautymusic.utils;

public enum PlaybackStatus {
    PLAYING,
    RESUMING,
    PAUSED
}
