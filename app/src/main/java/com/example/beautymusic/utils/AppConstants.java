package com.example.beautymusic.utils;

public final class AppConstants {

    public static final String DB_NAME = "beautymusic.db";
    public static final String PLAY_TYPE = "play_type";
    public static final int VIEW_TYPE_LIST = 1;
    public static final int VIEW_TYPE_GRID = 2;
    public static boolean isShuffle = true;
    public static boolean isRepeat = true;

    public static final String ACTION_PLAY = "com.example.beautymusic.service.ACTION_PLAY";
    public static final String ACTION_PAUSE = "com.example.beautymusic.service.ACTION_PAUSE";
    public static final String ACTION_PREVIOUS = "com.example.beautymusic.service.ACTION_PREVIOUS";
    public static final String ACTION_NEXT = "com.example.beautymusic.service.ACTION_NEXT";
    public static final String ACTION_STOP = "com.example.beautymusic.service.ACTION_STOP";

    public static final String Broadcast_PLAY_NEW_AUDIO = "com.example.beautymusic.ui.play.PLAY_NEW_AUDIO";
    public static final String Broadcast_PAUSE_AUDIO = "com.example.beautymusic.ui.play.PAUSE_AUDIO";
    public static final String Broadcast_RESUME_AUDIO = "com.example.beautymusic.ui.play.RESUME_AUDIO";
    public static final String Broadcast_PREVIOS_AUDIO = "com.example.beautymusic.ui.play.PREVIOS_AUDIO";
    public static final String Broadcast_NEXT_AUDIO = "com.example.beautymusic.ui.play.NEXT_AUDIO";
    public static final String Broadcast_ACTION_AUDIO = "com.example.beautymusic.ui.play.ACTION_AUDIO";
    public static final String Broadcast_LOAD_AUDIO = "com.example.beautymusic.ui.play.LOAD_AUDIO";

    public static final String Broadcast_SEEK_TO = "com.example.beautymusic.ui.play.SEEK_TO";
    public static final String Broadcast_SEND_DURATION = "com.example.beautymusic.ui.play.SEND_DURATION";
    public static final String Broadcast_SEND_CURRENT = "com.example.beautymusic.ui.play.SEND_CURRENT";
    public static final String Broadcast_MEDIA_CHANGE = "com.example.beautymusic.ui.play.MEDIA_CHANGE";

    public static final String Broadcast_SHOW_ALERT = "com.example.beautymusic.ui.play.SHOW_ALERT";
    public static final String Broadcast_PLAY_FOREGROUND = "com.example.beautymusic.ui.play.PLAY_FOREGROUND";
}
