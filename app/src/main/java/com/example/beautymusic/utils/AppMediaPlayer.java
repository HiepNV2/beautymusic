package com.example.beautymusic.utils;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.example.beautymusic.R;
import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.ui.play.PlayActivity;

import java.io.IOException;

public class AppMediaPlayer {

    private static MediaPlayer mMediaPlayer;
    private static Song mCurrentPlay;

    private  AppMediaPlayer() {

    }

    public static Song getCurrentPlay() {
        return mCurrentPlay;
    }

    public static void setCurrentPlay(Song currentPlay) {
        mCurrentPlay = currentPlay;
    }

    public static void setMediaPlayer(MediaPlayer mediaPlayer) {
        mMediaPlayer = mediaPlayer;
    }

    public static MediaPlayer getMediaPlayer() {
        return mMediaPlayer;
    }

}
