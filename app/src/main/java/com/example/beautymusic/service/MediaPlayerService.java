package com.example.beautymusic.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaMetadata;
import android.media.MediaPlayer;
import android.media.session.MediaController;
import android.media.session.MediaSession;
import android.media.session.MediaSessionManager;
import android.os.Build;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.example.beautymusic.R;
import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.ui.main.MainActivity;
import com.example.beautymusic.utils.AppConstants;
import com.example.beautymusic.utils.PlaybackStatus;
import com.example.beautymusic.utils.StorageUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class MediaPlayerService extends Service implements MediaPlayer.OnCompletionListener,
        MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnSeekCompleteListener,
        MediaPlayer.OnInfoListener, MediaPlayer.OnBufferingUpdateListener,
        AudioManager.OnAudioFocusChangeListener {

    private static final String TAG = "MediaPlayerService";

    //MediaSession
    private MediaSessionManager mediaSessionManager;
    private MediaSession mediaSession;
    private MediaController.TransportControls transportControls;

    //AudioPlayer notification ID
    private static final int NOTIFICATION_ID = 101;

    // Binder given to clients
    private MediaThread mMediaThread;
    private StorageUtil mStorage;

    private MediaPlayer mediaPlayer;
    private AudioManager audioManager;

    private int resumePosition;
    private boolean ongoingCall = false;
    private PhoneStateListener phoneStateListener;
    private TelephonyManager telephonyManager;
    private ArrayList<Song> audioList;
    private int audioIndex = -1;
    private Song activeAudio;
    private boolean isOnGoing = true;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        callStateListener();

        registerBecomingNoisyReceiver();

        registerAudioJackReceiver();

        register_loadAudio();

        register_actionAudio();

        register_playNewAudio();

        register_pauseAudio();

        register_resumeAudio();

        register_skipToPreviosAudio();

        register_skipToNextAudio();

        register_seekToPositionAudio();

        register_stopForegroundReceiver();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            // Load data from SharedPreferences
            mStorage = new StorageUtil(getApplicationContext());
            audioList = mStorage.loadAudio();
            audioIndex = mStorage.loadAudioIndex();

            if (audioIndex != -1 && audioIndex < audioList.size()) {
                // index is in a valid range
                activeAudio = audioList.get(audioIndex);
            } else {
                stopSelf();
            }
        } catch (NullPointerException e) {
            stopSelf();
        }

        // Request audio focus
        if (!requestAudioFocus()) {
            // Could not gain focus
            stopSelf();
        }

        if (mediaSessionManager == null) {
            try {
                initMediaSession();
                initMediaPlayer();
            } catch (RemoteException e) {
                e.printStackTrace();
                stopSelf();
            }
            buildNotification(PlaybackStatus.PLAYING);
        }

        // Handle Intent action from MediaSession.TransportControls
        handleIncomingActions(intent);

        return START_NOT_STICKY;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);

    }

    @Override
    public void onDestroy() {

        if (mediaPlayer != null) {
            stopMedia();
            mediaPlayer.release();
        }
        removeAudioFocus();

        //Disable the PhoneStateListener
        if (phoneStateListener != null) {
            telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
        }

        removeNotification();

        //unregister BroadcastReceivers
        unregisterReceiver(becomingNoisyReceiver);
        unregisterReceiver(audioJackReceiver);
        unregisterReceiver(loadAudio);
        unregisterReceiver(actionAudio);
        unregisterReceiver(playNewAudio);
        unregisterReceiver(pauseAudio);
        unregisterReceiver(resumeAudio);
        unregisterReceiver(skipToPreviousAudio);
        unregisterReceiver(skipToNextAudio);
        unregisterReceiver(seekToPositionAudio);
        unregisterReceiver(stopForegroundReceiver);

        //clear cached playlist
//        new StorageUtil(getApplicationContext()).clearCachedAudioPlaylist();

    }

    private class MediaThread extends Thread {
        @Override
        public void run() {
            while (mediaPlayer.isPlaying()) {
                try {
                    int current = mediaPlayer.getCurrentPosition() / 1000;

                    sendMediaCurrentPosition(current);

                    Thread.currentThread().sleep(1000);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        audioList = mStorage.loadAudio();
        audioIndex = mStorage.loadAudioIndex();
        skipToNext();
        updateMetaData();
        buildNotification(PlaybackStatus.PLAYING);
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        switch (what) {
            case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
                Log.d("MediaPlayer Error", "MEDIA ERROR NOT VALID FOR PROGRESSIVE PLAYBACK " + extra);
                break;
            case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                Log.d("MediaPlayer Error", "MEDIA ERROR SERVER DIED " + extra);
                break;
            case MediaPlayer.MEDIA_ERROR_UNKNOWN:
                Log.d("MediaPlayer Error", "MEDIA ERROR UNKNOWN " + extra);
                break;
        }
        return false;
    }

    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        playMedia();
    }

    @Override
    public void onSeekComplete(MediaPlayer mp) {

    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                // resume playback
                if (mediaPlayer == null) initMediaPlayer();
                else if (!mediaPlayer.isPlaying()) mediaPlayer.start();
                mediaPlayer.setVolume(1.0f, 1.0f);
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
                if (mediaPlayer.isPlaying()) mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                if (mediaPlayer.isPlaying()) mediaPlayer.pause();
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                if (mediaPlayer.isPlaying()) mediaPlayer.setVolume(0.1f, 0.1f);
                break;
        }
    }

    // Media player
    private void initMediaPlayer() {

        if (mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
        }

        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.setOnErrorListener(this);
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnBufferingUpdateListener(this);
        mediaPlayer.setOnSeekCompleteListener(this);
        mediaPlayer.setOnInfoListener(this);

        mediaPlayer.reset();

        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        try {
            mediaPlayer.setDataSource(activeAudio.getPath());

        } catch (IOException e) {
            e.printStackTrace();
            stopSelf();
        }
        mediaPlayer.prepareAsync();
    }

    private void playMedia() {
        if (!mediaPlayer.isPlaying()) {
            mediaPlayer.start();
            // send media change
            sendMediaPlayerChange();
            // send playback status
            sendPlaybackStatus(PlaybackStatus.PLAYING);
            // send duration
            sendMediaDuration(mediaPlayer.getDuration() / 1000);
            // send current position after 1s

            mMediaThread = new MediaThread();
            mMediaThread.start();
            isOnGoing = true;
        }
    }

    private void stopMedia() {
        if (mediaPlayer == null) return;
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            if (mMediaThread != null) {
                mMediaThread.interrupt();
            }
            isOnGoing = false;
        }
    }

    private void pauseMedia() {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            resumePosition = mediaPlayer.getCurrentPosition();
            if (mMediaThread != null) {
                mMediaThread.interrupt();
            }
            isOnGoing = false;
        }
    }

    private void resumeMedia() {
        if (!mediaPlayer.isPlaying()) {
            mediaPlayer.seekTo(resumePosition);
            mediaPlayer.start();
            mMediaThread = new MediaThread();
            mMediaThread.start();
            isOnGoing = true;
        }
    }

    private void sendMediaDuration(int duration) {
        Intent intent = new Intent(AppConstants.Broadcast_SEND_DURATION);
        intent.putExtra("duration", duration);
        LocalBroadcastManager.getInstance(MediaPlayerService.this).sendBroadcast(intent);
    }

    private void sendMediaCurrentPosition(int current) {
        Intent intent = new Intent(AppConstants.Broadcast_SEND_CURRENT);
        intent.putExtra("currentPosition", current);
        LocalBroadcastManager.getInstance(MediaPlayerService.this).sendBroadcast(intent);
    }

    private void sendMediaPlayerChange() {
        Intent intent = new Intent(AppConstants.Broadcast_MEDIA_CHANGE);
        LocalBroadcastManager.getInstance(MediaPlayerService.this).sendBroadcast(intent);
    }

    private void sendPlaybackStatus(PlaybackStatus playbackStatus) {
        Intent intent = new Intent(AppConstants.Broadcast_ACTION_AUDIO);
        intent.putExtra("playback_status", playbackStatus);
        LocalBroadcastManager.getInstance(MediaPlayerService.this).sendBroadcast(intent);
    }

    private void sendAlertRepeatIsOff() {
        Intent intent = new Intent(AppConstants.Broadcast_SHOW_ALERT);
        intent.putExtra("alert", "Phát lặp lại chưa được bật!");
        LocalBroadcastManager.getInstance(MediaPlayerService.this).sendBroadcast(intent);
    }

    // Audio focus
    private boolean requestAudioFocus() {
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int result = audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            //Focus gained
            return true;
        }
        return false;
    }

    private boolean removeAudioFocus() {
        return AudioManager.AUDIOFOCUS_REQUEST_GRANTED ==
                audioManager.abandonAudioFocus(this);
    }

    // Handle incoming phone calls
    private void callStateListener() {

        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        phoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                switch (state) {
                    case TelephonyManager.CALL_STATE_OFFHOOK:
                    case TelephonyManager.CALL_STATE_RINGING:
                        if (mediaPlayer != null) {
                            pauseMedia();
                            ongoingCall = true;
                        }
                        break;
                    case TelephonyManager.CALL_STATE_IDLE:
                        if (mediaPlayer != null) {
                            if (ongoingCall) {
                                ongoingCall = false;
                                resumeMedia();
                            }
                        }
                        break;
                }
            }
        };
        telephonyManager.listen(phoneStateListener,
                PhoneStateListener.LISTEN_CALL_STATE);
    }

    // Declare broadcast receiver
    private BroadcastReceiver becomingNoisyReceiver = new BroadcastReceiver() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onReceive(Context context, Intent intent) {
            pauseMedia();
            buildNotification(PlaybackStatus.PAUSED);
        }
    };

    private BroadcastReceiver audioJackReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
                int state = intent.getIntExtra("state", -1);
                switch (state) {
                    case 0:
                        Log.d(TAG, "Headset is unplugged");
                        pauseMedia();
                        stopForeground(false);
                        updateMetaData();
                        buildNotification(PlaybackStatus.PAUSED);
                        break;
                    case 1:
                        Log.d(TAG, "Headset is plugged");
                        playMedia();
                        updateMetaData();
                        buildNotification(PlaybackStatus.PLAYING);
                        break;
                    default:
                        Log.d(TAG, "I have no idea what the headset state is");
                }
            }
        }
    };

    private BroadcastReceiver playNewAudio = new BroadcastReceiver() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get the new media index form SharedPreferences
            audioIndex = mStorage.loadAudioIndex();
            if (audioIndex != -1 && audioIndex < audioList.size()) {
                activeAudio = audioList.get(audioIndex);
            } else {
                stopSelf();
            }

            // reset mediaPlayer to play the new Audio
            stopMedia();
            mediaPlayer.reset();
            initMediaPlayer();
            updateMetaData();
            buildNotification(PlaybackStatus.PLAYING);
        }
    };

    private BroadcastReceiver loadAudio = new BroadcastReceiver() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onReceive(Context context, Intent intent) {
            sendMediaPlayerChange();
            sendMediaDuration(mediaPlayer.getDuration() / 1000);
            sendMediaCurrentPosition(mediaPlayer.getCurrentPosition() / 1000);
            sendPlaybackStatus(mediaPlayer.isPlaying()
                    ? PlaybackStatus.PLAYING : PlaybackStatus.PAUSED);

        }
    };

    private BroadcastReceiver actionAudio = new BroadcastReceiver() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onReceive(Context context, Intent intent) {
            if (mediaPlayer.isPlaying()) {
                pauseMedia();
                buildNotification(PlaybackStatus.PAUSED);
                sendPlaybackStatus(PlaybackStatus.PAUSED);

            } else {
                resumeMedia();
                buildNotification(PlaybackStatus.PLAYING);
                sendPlaybackStatus(PlaybackStatus.PLAYING);
            }
        }
    };

    private BroadcastReceiver pauseAudio = new BroadcastReceiver() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onReceive(Context context, Intent intent) {
            pauseMedia();
            buildNotification(PlaybackStatus.PAUSED);
        }
    };

    private BroadcastReceiver resumeAudio = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            resumeMedia();
            buildNotification(PlaybackStatus.PLAYING);
        }
    };

    private BroadcastReceiver skipToPreviousAudio = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            skipToPrevious();
            updateMetaData();
            buildNotification(PlaybackStatus.PLAYING);
        }
    };

    private BroadcastReceiver skipToNextAudio = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            skipToNext();
            updateMetaData();
            buildNotification(PlaybackStatus.PLAYING);
        }
    };

    private BroadcastReceiver seekToPositionAudio = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int position = intent.getIntExtra("seekTo", 0);
            seekToPositionAudio(position);
        }
    };

    private BroadcastReceiver stopForegroundReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            stopForeground(true);
            stopSelf();
        }
    };

    // Register broadcast receiver
    private void registerBecomingNoisyReceiver() {
        IntentFilter intentFilter = new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
        registerReceiver(becomingNoisyReceiver, intentFilter);
    }

    private void registerAudioJackReceiver() {
        IntentFilter intentFilter = new IntentFilter(AudioManager.ACTION_HEADSET_PLUG);
        registerReceiver(audioJackReceiver, intentFilter);
    }

    private void register_loadAudio() {
        IntentFilter filter = new IntentFilter(AppConstants.Broadcast_LOAD_AUDIO);
        registerReceiver(loadAudio, filter);
    }

    private void register_actionAudio() {
        IntentFilter filter = new IntentFilter(AppConstants.Broadcast_ACTION_AUDIO);
        registerReceiver(actionAudio, filter);
    }

    private void register_playNewAudio() {
        IntentFilter filter = new IntentFilter(AppConstants.Broadcast_PLAY_NEW_AUDIO);
        registerReceiver(playNewAudio, filter);
    }

    private void register_pauseAudio() {
        IntentFilter filter = new IntentFilter(AppConstants.Broadcast_PAUSE_AUDIO);
        registerReceiver(pauseAudio, filter);
    }

    private void register_resumeAudio() {
        IntentFilter filter = new IntentFilter(AppConstants.Broadcast_RESUME_AUDIO);
        registerReceiver(resumeAudio, filter);
    }

    private void register_skipToPreviosAudio() {
        IntentFilter filter = new IntentFilter(AppConstants.Broadcast_PREVIOS_AUDIO);
        registerReceiver(skipToPreviousAudio, filter);
    }

    private void register_skipToNextAudio() {
        IntentFilter filter = new IntentFilter(AppConstants.Broadcast_NEXT_AUDIO);
        registerReceiver(skipToNextAudio, filter);
    }

    private void register_seekToPositionAudio() {
        IntentFilter filter = new IntentFilter(AppConstants.Broadcast_SEEK_TO);
        registerReceiver(seekToPositionAudio, filter);
    }

    private void register_stopForegroundReceiver() {
        IntentFilter filter = new IntentFilter(AppConstants.Broadcast_PLAY_FOREGROUND);
        registerReceiver(stopForegroundReceiver, filter);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void initMediaSession() throws RemoteException {
        if (mediaSessionManager != null) return; // mediaSessionManager exists

        mediaSessionManager = (MediaSessionManager) getSystemService(Context.MEDIA_SESSION_SERVICE);

        mediaSession = new MediaSession(getApplicationContext(), "AudioPlayer");

        transportControls = mediaSession.getController().getTransportControls();

        //set MediaSession -> ready to receive media commands
        mediaSession.setActive(true);

        mediaSession.setFlags(MediaSession.FLAG_HANDLES_TRANSPORT_CONTROLS);

        //Set mediaSession's MetaData
        updateMetaData();

        // Attach Callback to receive MediaSession updates
        mediaSession.setCallback(new MediaSession.Callback() {
            // Implement callbacks
            @Override
            public void onPlay() {
                super.onPlay();
                resumeMedia();
                buildNotification(PlaybackStatus.PLAYING);
            }

            @Override
            public void onPause() {
                super.onPause();
                pauseMedia();
                buildNotification(PlaybackStatus.PAUSED);
            }

            @Override
            public void onSkipToNext() {
                super.onSkipToNext();
                skipToNext();
                updateMetaData();
                buildNotification(PlaybackStatus.PLAYING);
            }

            @Override
            public void onSkipToPrevious() {
                super.onSkipToPrevious();
                skipToPrevious();
                updateMetaData();
                buildNotification(PlaybackStatus.PLAYING);
            }

            @Override
            public void onStop() {
                super.onStop();
                removeNotification();
                //Stop the service
                stopSelf();
            }

            @Override
            public void onSeekTo(long position) {
                super.onSeekTo(position);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void updateMetaData() {
        Bitmap albumArt = BitmapFactory.decodeResource(getResources(),
                R.drawable.bg_album);

        // Update the current metadata
        if (activeAudio != null) {
            mediaSession.setMetadata(new MediaMetadata.Builder()
                    .putBitmap(MediaMetadata.METADATA_KEY_ALBUM_ART, albumArt)
                    .putString(MediaMetadata.METADATA_KEY_ARTIST, activeAudio.getArtist())
                    .putString(MediaMetadata.METADATA_KEY_ALBUM, activeAudio.getAlbum())
                    .putString(MediaMetadata.METADATA_KEY_TITLE, activeAudio.getName())
                    .build());
        }
    }

    private void skipToNext() {
        // if shuffle is on
        if (AppConstants.isShuffle) {
            Random r = new Random();
            audioIndex = r.nextInt(audioList.size());
            activeAudio = audioList.get(audioIndex);
        } else {
            // if last in playlist
            if (audioIndex == audioList.size() - 1) {
                // if repeat is on
                if (AppConstants.isRepeat) {
                    audioIndex = 0;
                    activeAudio = audioList.get(audioIndex);
                } else {
                    playMedia();
                    sendAlertRepeatIsOff();
                }
            } else {
                // get next in playlist
                activeAudio = audioList.get(++audioIndex);
            }
        }

        // Update stored index
        new StorageUtil(getApplicationContext()).storeAudioIndex(audioIndex);

        stopMedia();
        // reset mediaPlayer
        mediaPlayer.reset();
        initMediaPlayer();
    }

    private void skipToPrevious() {
        if (AppConstants.isShuffle) {
            Random r = new Random();
            audioIndex = r.nextInt(audioList.size());
            activeAudio = audioList.get(audioIndex);
        } else {
            // if first in playlist
            if (audioIndex == 0) {
                // if repeat is on
                if (AppConstants.isRepeat) {
                    audioIndex = audioList.size() - 1;
                    activeAudio = audioList.get(audioIndex);
                } else {
                    playMedia();
                    sendAlertRepeatIsOff();
                }
            } else {
                //get previous in playlist
                activeAudio = audioList.get(--audioIndex);
            }
        }

        //Update stored index
        new StorageUtil(getApplicationContext()).storeAudioIndex(audioIndex);

        stopMedia();
        //reset mediaPlayer
        mediaPlayer.reset();
        initMediaPlayer();
    }

    private void seekToPositionAudio(int position) {
        mediaPlayer.seekTo(position);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void buildNotification(PlaybackStatus playbackStatus) {

        int notificationAction = android.R.drawable.ic_media_pause;
        PendingIntent play_pauseAction = null;

        // Build a new notification according to the current state of the MediaPlayer
        if (playbackStatus == PlaybackStatus.PLAYING) {
            notificationAction = android.R.drawable.ic_media_pause;
            //create the pause action
            play_pauseAction = playbackAction(1);
        } else if (playbackStatus == PlaybackStatus.PAUSED) {
            notificationAction = android.R.drawable.ic_media_play;
            //create the play action
            play_pauseAction = playbackAction(0);
        }

        // Content intent
        Intent notificationIntent = new Intent(this, MainActivity.class);

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        // Create a new Notification
        Notification.Builder notificationBuilder = new Notification.Builder(this)
                .setShowWhen(false)
                .setOngoing(isOnGoing)
                .setStyle(new Notification.MediaStyle()
                        .setMediaSession(mediaSession.getSessionToken())
                        .setShowActionsInCompactView(0, 1, 2))
                .setColor(Color.argb(100, 0, 172, 189))
                .setSmallIcon(R.drawable.ic_beat)
                .setContentTitle(activeAudio.getName())
                .setContentText(activeAudio.getArtist())
                .setContentIntent(contentIntent)
                // Add playback actions
                .addAction(android.R.drawable.ic_media_previous, "previous", playbackAction(3))
                .addAction(notificationAction, "pause", play_pauseAction)
                .addAction(android.R.drawable.ic_media_next, "next", playbackAction(2));

        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(NOTIFICATION_ID, notificationBuilder.build());

        if (mediaPlayer.isPlaying()) {
            startForeground(NOTIFICATION_ID, notificationBuilder.getNotification());
        }
    }

    private void removeNotification() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);
    }

    private PendingIntent playbackAction(int actionNumber) {
        Intent playbackAction = new Intent(this, MediaPlayerService.class);
        switch (actionNumber) {
            case 0:
                playbackAction.setAction(AppConstants.ACTION_PLAY);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            case 1:
                playbackAction.setAction(AppConstants.ACTION_PAUSE);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            case 2:
                playbackAction.setAction(AppConstants.ACTION_NEXT);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            case 3:
                playbackAction.setAction(AppConstants.ACTION_PREVIOUS);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            default:
                break;
        }
        return null;
    }

    private void handleIncomingActions(Intent playbackAction) {
        if (playbackAction == null || playbackAction.getAction() == null) return;

        String actionString = playbackAction.getAction();
        if (actionString.equalsIgnoreCase(AppConstants.ACTION_PLAY)) {
            transportControls.play();
            sendPlaybackStatus(PlaybackStatus.PLAYING);

        } else if (actionString.equalsIgnoreCase(AppConstants.ACTION_PAUSE)) {
            transportControls.pause();
            stopForeground(false);
            sendPlaybackStatus(PlaybackStatus.PAUSED);

        } else if (actionString.equalsIgnoreCase(AppConstants.ACTION_NEXT)) {
            transportControls.skipToNext();

        } else if (actionString.equalsIgnoreCase(AppConstants.ACTION_PREVIOUS)) {
            transportControls.skipToPrevious();

        } else if (actionString.equalsIgnoreCase(AppConstants.ACTION_STOP)) {
            transportControls.stop();
        }
    }


}
