package com.example.beautymusic.ui.main.artists;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import com.example.beautymusic.data.db.model.Album;
import com.example.beautymusic.data.db.model.Artist;
import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.ui.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ArtistPresenter<V extends ArtistMvpView,
        I extends ArtistMvpInteractor> extends BasePresenter<V, I>
        implements ArtistMvpPresenter<V, I> {

    private static final String TAG = "ArtistPresenter";
    private List<Artist> mArtistList;

    @Inject
    public ArtistPresenter(I mvpInteractor) {
        super(mvpInteractor);
        mArtistList = new ArrayList<>();
    }

    @Override
    public void getAllArtistFromDevice(Context context) {
        Uri uri = MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI;

        String[] projection = new String[]{
                MediaStore.Audio.Artists._ID,
                MediaStore.Audio.Artists.ARTIST,
                MediaStore.Audio.Artists.NUMBER_OF_ALBUMS,
                MediaStore.Audio.Artists.NUMBER_OF_TRACKS,
        };

        String sortOrder = MediaStore.Audio.Media.ARTIST + " ASC";

        Cursor artistCursor = context.getContentResolver().query(uri,
                projection,
                null,
                null,
                sortOrder
        );

        if (artistCursor != null) {
            while (artistCursor.moveToNext()) {

                Artist artist = new Artist();
                String id = artistCursor.getString(0);
                String name = artistCursor.getString(1);
                int numAlbums = artistCursor.getInt(2);
                int numSongs = artistCursor.getInt(3);

                artist.setArtistId(id);
                artist.setArtistName(name);
                artist.setNumberAlbums(numAlbums);
                artist.setNumberSongs(numSongs);

                mArtistList.add(artist);
            }
            artistCursor.close();

            Log.d(TAG, "mArtistList: " + mArtistList.size());
            getMvpView().updateArtist(mArtistList);
        }
    }

    @Override
    public void onIntentPrepared(int position) {
        getMvpView().sendIntentArtistDetail(mArtistList.get(position));
    }
}
