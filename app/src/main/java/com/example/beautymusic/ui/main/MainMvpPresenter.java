package com.example.beautymusic.ui.main;

import android.os.Bundle;

import com.example.beautymusic.ui.base.MvpInteractor;
import com.example.beautymusic.ui.base.MvpPresenter;
import com.example.beautymusic.ui.base.MvpView;

public interface MainMvpPresenter<V extends MvpView, I extends MvpInteractor>
        extends MvpPresenter<V, I> {

}
