package com.example.beautymusic.ui.main.artists;

import com.example.beautymusic.data.db.model.Artist;
import com.example.beautymusic.ui.base.MvpView;

import java.util.List;

public interface ArtistMvpView extends MvpView {

    void updateArtist(List<Artist> artistList);

    void sendIntentArtistDetail(Artist artist);
    
}
