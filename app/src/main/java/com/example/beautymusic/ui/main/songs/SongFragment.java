package com.example.beautymusic.ui.main.songs;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.beautymusic.R;
import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.di.component.ActivityComponent;
import com.example.beautymusic.ui.base.BaseFragment;
import com.example.beautymusic.ui.main.MainActivity;
import com.example.beautymusic.ui.main.artists.ArtistAdapter;
import com.example.beautymusic.ui.play.PlayActivity;
import com.example.beautymusic.utils.AppConstants;
import com.example.beautymusic.utils.PlaybackStatus;
import com.example.beautymusic.utils.StorageUtil;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

public class SongFragment extends BaseFragment implements SongMvpView {

    private static final String TAG = "SongFragment";
    private static final int PERMISSION_REQUEST_CODE = 1;

    @Inject
    SongMvpPresenter<SongMvpView, SongMvpInteractor> mPresenter;

    @Inject
    SongAdapter mSongAdapter;

    @Inject
    LinearLayoutManager mLinearLayoutManager;

    @Inject
    GridLayoutManager mGridLayoutManager;

    @Inject
    StorageUtil mStorage;

    private RecyclerView mRcvSong;

    public static SongFragment newInstance() {
        Bundle args = new Bundle();
        SongFragment fragment = new SongFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_song, container, false);

        ActivityComponent component = getActivityComponent();

        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
        }

        return view;
    }

    @Override
    public void onResume() {
        setOnClickAdapter();
        if (mStorage.loadAudio() != null) {
            mPresenter.onLoadData(mStorage.loadAudio());
        }
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnu_view_list:
                changeViewList();
                return true;
            case R.id.mnu_view_grid:
                changeViewGrid();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void setUp(View view) {
        mRcvSong = (RecyclerView) view.findViewById(R.id.rv_songs);

        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRcvSong.setLayoutManager(mLinearLayoutManager);
        mRcvSong.setItemAnimator(new DefaultItemAnimator());
        mRcvSong.setAdapter(mSongAdapter);
        setOnClickAdapter();

        if (Build.VERSION.SDK_INT >= 23) {
            if (hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                mPresenter.getAllAudioFromDevice(getContext());
            } else {
                requestPermissionsSafely(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        PERMISSION_REQUEST_CODE);
            }
        } else {
            mPresenter.getAllAudioFromDevice(getContext());
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionsSafely(String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                getContext().checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "Permission Granted.");
                    mPresenter.getAllAudioFromDevice(getContext());
                } else {
                    Log.e(TAG, "Permission Denied.");
                }
                break;
        }
    }

    @Override
    public void updateSong(List<Song> songList) {
        if (mSongAdapter.getAudioList() != null) {
            mSongAdapter.clearData();
        }
        mSongAdapter.addItems(songList);
        mStorage.storeAudio(songList);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void startIntentPlay(List<Song> audioList, int audioIndex) {
        mStorage.storeAudio(audioList);
        mStorage.storeAudioIndex(audioIndex);

        startActivity(PlayActivity.getStartIntent(
                this.getContext()).putExtra(AppConstants.PLAY_TYPE, PlaybackStatus.PLAYING));
    }

    private void setOnClickAdapter() {
        mSongAdapter.setOnItemClickListener(new SongAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(View view, int position) {
                mPresenter.onPlayIntentPrepared(position);
            }
        });
    }

    private void changeViewList() {
        mSongAdapter.setViewType(AppConstants.VIEW_TYPE_LIST);
        mRcvSong.setLayoutManager(mLinearLayoutManager);
        mRcvSong.setAdapter(mSongAdapter);
    }

    private void changeViewGrid() {
        mSongAdapter.setViewType(AppConstants.VIEW_TYPE_GRID);
        mRcvSong.setLayoutManager(mGridLayoutManager);
        mRcvSong.setAdapter(mSongAdapter);
    }
}
