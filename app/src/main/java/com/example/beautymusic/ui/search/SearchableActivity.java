package com.example.beautymusic.ui.search;

import android.app.Activity;
import android.app.ListActivity;
import android.app.SearchManager;
import android.app.Service;
import android.content.AsyncQueryHandler;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beautymusic.R;
import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.ui.base.BaseActivity;
import com.example.beautymusic.ui.main.albums.detail.AlbumDetailActivity;
import com.example.beautymusic.ui.main.artists.detail.ArtistDetailActivity;
import com.example.beautymusic.ui.main.songs.SongAdapter;
import com.example.beautymusic.ui.play.PlayActivity;
import com.example.beautymusic.utils.AppConstants;
import com.example.beautymusic.utils.PlaybackStatus;
import com.example.beautymusic.utils.StorageUtil;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class SearchableActivity extends BaseActivity {

    @Inject
    StorageUtil mStorage;

    private static final String TAG = "SearchableActivity";

    private MyHandler mHandler;

    private TextView txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchable);

        getActivityComponent().inject(this);

        bindView();

        setUp();
    }

    @Override
    protected void bindView() {
        txt = (TextView) findViewById(R.id.textView);
    }

    @Override
    protected void setUp() {
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);

        } else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            mHandler = new MyHandler(this);
            mHandler.startQuery(0, null, intent.getData(), null, null, null, null);
            Log.d("DATA", intent.getDataString());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void prepareIntent(String contentType, String data) {
        Log.d(TAG, contentType + " | " + data);
        switch (contentType) {
            case "SONG":
                Log.d(TAG, "playIntent");
                storeAudioIndex(data);
                Intent playIntent = PlayActivity.getStartIntent(this);
                playIntent.putExtra(AppConstants.PLAY_TYPE, PlaybackStatus.PLAYING);
                startActivity(playIntent);
                finish();
                break;
            case "ALBUM":
                Log.d(TAG, "startAlbumIntent");
                Intent albumDetailIntent = AlbumDetailActivity.getStartIntent(this);
                albumDetailIntent.putExtra("ALBUM_ID", data);
                startActivity(albumDetailIntent);
                finish();
                break;
            case "ARTIST":
                Log.d(TAG, "artistDetailIntent");
                Intent artistDetailIntent = ArtistDetailActivity.getStartIntent(this);
                artistDetailIntent.putExtra("ARTIST_ID", data);
                startActivity(artistDetailIntent);
                finish();
                break;
        }
    }

    private void storeAudioIndex(String data) {
        List<Song> songList = mStorage.loadAudio();
        for (Song song : songList) {
            if (song.getId().equals(data)) {
                mStorage.storeAudioIndex(songList.indexOf(song));
            }
        }
    }

    public void updateText(String text) {
        txt.setText(text);
    }

    static class MyHandler extends AsyncQueryHandler {
        // avoid memory leak
        WeakReference<SearchableActivity> activity;

        private MyHandler(SearchableActivity searchableActivity) {
            super(searchableActivity.getContentResolver());
            activity = new WeakReference<>(searchableActivity);
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
            super.onQueryComplete(token, cookie, cursor);
            if (cursor == null || cursor.getCount() == 0) return;

            cursor.moveToFirst();

            long id = cursor.getLong(cursor.getColumnIndex(BaseColumns._ID));
            String text = cursor.getString(cursor.getColumnIndex(SearchManager.SUGGEST_COLUMN_TEXT_1));
            long data_id = cursor.getLong(cursor.getColumnIndex(SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID));
            String contentType = cursor.getString(cursor.getColumnIndex(SearchManager.SUGGEST_COLUMN_CONTENT_TYPE));
            String data = cursor.getString(cursor.getColumnIndex(SearchManager.SUGGEST_URI_PATH_QUERY));

            cursor.close();

            if (activity.get() != null) {
//                activity.get().updateText("onQueryComplete: " + id + " / " + text + " / " + data_id + " / " + contentType + " / " + data);
                activity.get().prepareIntent(contentType, data);
            }
        }
    }

}
