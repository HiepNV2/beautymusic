package com.example.beautymusic.ui.main.albums;

import com.example.beautymusic.data.db.model.Album;
import com.example.beautymusic.ui.base.MvpView;

import java.util.List;

public interface AlbumMvpView extends MvpView {

    void updateAlbum(List<Album> albumList);

    void sendIntentAlbumDetail(Album album);
}
