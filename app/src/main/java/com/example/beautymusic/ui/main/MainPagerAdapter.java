package com.example.beautymusic.ui.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.beautymusic.ui.main.albums.AlbumFragment;
import com.example.beautymusic.ui.main.artists.ArtistFragment;
import com.example.beautymusic.ui.main.songs.SongFragment;

public class MainPagerAdapter extends FragmentStatePagerAdapter {

    private int mTabCount;

    public MainPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
        this.mTabCount = 0;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return SongFragment.newInstance();
            case 1:
                return AlbumFragment.newInstance();
            case 2:
                return ArtistFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mTabCount;
    }

    public void setCount(int count) {
        mTabCount = count;
    }
}
