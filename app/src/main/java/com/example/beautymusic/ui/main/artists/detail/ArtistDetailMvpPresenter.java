package com.example.beautymusic.ui.main.artists.detail;

import android.content.Context;

import com.example.beautymusic.data.db.model.Artist;
import com.example.beautymusic.ui.base.MvpInteractor;
import com.example.beautymusic.ui.base.MvpPresenter;
import com.example.beautymusic.ui.base.MvpView;

public interface ArtistDetailMvpPresenter<V extends MvpView, I extends MvpInteractor>
        extends MvpPresenter<V, I> {

    void getArtistInfo(Context context, String artistId);

    void getSongsPerArtist(Context context, String artistId);

    void onIntentPrepared(int position);
}
