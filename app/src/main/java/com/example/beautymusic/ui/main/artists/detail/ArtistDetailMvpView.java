package com.example.beautymusic.ui.main.artists.detail;

import com.example.beautymusic.data.db.model.Album;
import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.ui.base.MvpView;

import java.util.List;

public interface ArtistDetailMvpView extends MvpView {

    void setArtistName(String name);

    void updatePlayList(List<Song> songList);

    void startIntentPlay(List<Song> audioList, int audioIndex);

}
