package com.example.beautymusic.ui.play.queue;

import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.ui.base.MvpView;

import java.util.List;

public interface QueueMvpView extends MvpView {

    void updatePlayList(List<Song> songList);

}
