package com.example.beautymusic.ui.main.artists;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.beautymusic.R;
import com.example.beautymusic.data.db.model.Artist;
import com.example.beautymusic.di.component.ActivityComponent;
import com.example.beautymusic.ui.base.BaseFragment;
import com.example.beautymusic.ui.main.albums.AlbumAdapter;
import com.example.beautymusic.ui.main.artists.detail.ArtistDetailActivity;
import com.example.beautymusic.utils.AppConstants;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

public class ArtistFragment extends BaseFragment implements ArtistMvpView{

    @Inject
    ArtistMvpPresenter<ArtistMvpView, ArtistMvpInteractor> mPresenter;

    @Inject
    ArtistAdapter mArtistAdapter;

    @Inject
    LinearLayoutManager mLinearLayoutManager;

    @Inject
    GridLayoutManager mGridLayoutManager;

    private static final String TAG = "ArtistFragment";
    private static final int PERMISSION_REQUEST_CODE = 1;

    private RecyclerView mRcvArtist;

    public static ArtistFragment newInstance() {
        Bundle args = new Bundle();
        ArtistFragment fragment = new ArtistFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_artist, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null){
            component.inject(this);
            mPresenter.onAttach(this);
        }

        return view;
    }

    @Override
    public void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnu_view_list:
                changeViewList();
                return true;
            case R.id.mnu_view_grid:
                changeViewGrid();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void setUp(View view) {
        mRcvArtist = (RecyclerView) view.findViewById(R.id.rv_artists);

        mGridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRcvArtist.setLayoutManager(mGridLayoutManager);
        mRcvArtist.setItemAnimator(new DefaultItemAnimator());
        mRcvArtist.setAdapter(mArtistAdapter);

        mArtistAdapter.setOnItemClickListener(new ArtistAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(View view, int position) {
                mPresenter.onIntentPrepared(position);
            }
        });

        if (Build.VERSION.SDK_INT >= 23) {
            if (hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                mPresenter.getAllArtistFromDevice(getContext());
            } else {
                requestPermissionsSafely(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        PERMISSION_REQUEST_CODE);
            }
        } else {
            mPresenter.getAllArtistFromDevice(getContext());
        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionsSafely(String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                getContext().checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "Permission Granted.");
                    mPresenter.getAllArtistFromDevice(getContext());
                } else {
                    Log.e(TAG, "Permission Denied.");
                }
                break;
        }
    }

    @Override
    public void updateArtist(List<Artist> artistList) {
        mArtistAdapter.addItems(artistList);
    }

    @Override
    public void sendIntentArtistDetail(Artist artist) {
        Intent artistIntent = ArtistDetailActivity.getStartIntent(this.getContext());
        artistIntent.putExtra("ARTIST_ID", artist.getArtistId());
        getContext().startActivity(artistIntent);
    }

    private void changeViewList() {
        mArtistAdapter.setViewType(AppConstants.VIEW_TYPE_LIST);
        mRcvArtist.setLayoutManager(mLinearLayoutManager);
        mRcvArtist.setAdapter(mArtistAdapter);
    }

    private void changeViewGrid() {
        mArtistAdapter.setViewType(AppConstants.VIEW_TYPE_GRID);
        mRcvArtist.setLayoutManager(mGridLayoutManager);
        mRcvArtist.setAdapter(mArtistAdapter);
    }
}
