package com.example.beautymusic.ui.main.artists.detail;

import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.support.v7.widget.SearchView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.beautymusic.R;
import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.ui.base.BaseActivity;
import com.example.beautymusic.ui.main.songs.SongAdapter;
import com.example.beautymusic.ui.play.PlayActivity;
import com.example.beautymusic.ui.search.SearchableActivity;
import com.example.beautymusic.utils.AppConstants;
import com.example.beautymusic.utils.PlaybackStatus;
import com.example.beautymusic.utils.StorageUtil;

import java.util.List;

import javax.inject.Inject;

public class ArtistDetailActivity extends BaseActivity implements ArtistDetailMvpView {

    private static final String TAG = "ArtistDetailActivity";
    @Inject
    ArtistDetailMvpPresenter<ArtistDetailMvpView, ArtistDetailMvpInteractor> mPresenter;

    @Inject
    SongAdapter mSongAdapter;

    @Inject
    LinearLayoutManager mSongLayoutManager;

    @Inject
    StorageUtil mStorage;

    private Toolbar mTbArtistDetail;
    private CollapsingToolbarLayout mCtlArtistDetail;
    private RecyclerView mRcvSongOfArtist;
    private ImageView mIvSongControl, mIvSongPicture;
    private TextView mTvSongName, mTvArtitst;
    private AppCompatSeekBar mSbDuration;
    private LinearLayout mLayoutPlayBottom;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, ArtistDetailActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_detail);

        getActivityComponent().inject(this);

        mPresenter.onAttach(this);

        bindView();

        setUp();

        register_getMediaChange();

        register_getMediaDuration();

        register_getMediaCurrentPosition();

        register_getPlaybackStatus();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMediaChangeReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMediaDurationReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMediaCurrentPositionReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mPlaybackStatusReceiver);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem searchItem = menu.findItem(R.id.mnu_search);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(
                new ComponentName(this, SearchableActivity.class)));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            case R.id.mnu_sort_list:
                return true;
            case R.id.mnu_view_grid:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void bindView() {
        mTbArtistDetail = (Toolbar) findViewById(R.id.toolbar_artist_detail);
        mCtlArtistDetail = (CollapsingToolbarLayout)
                findViewById(R.id.collapsing_toolbar_artist_detail);
        mRcvSongOfArtist = (RecyclerView) findViewById(R.id.rv_songs_of_artist);
        mIvSongControl = (ImageView) findViewById(R.id.iv_song_control);
        mIvSongPicture = (ImageView) findViewById(R.id.iv_song_picture);
        mTvSongName = (TextView) findViewById(R.id.tv_song_name);
        mTvArtitst = (TextView) findViewById(R.id.tv_song_artist);
        mSbDuration = (AppCompatSeekBar) findViewById(R.id.sb_song_duration);
        mLayoutPlayBottom = (LinearLayout) findViewById(R.id.ll_song_control);
    }

    @Override
    protected void setUp() {
        setSupportActionBar(mTbArtistDetail);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mCtlArtistDetail.setTitle(getString(R.string.artist_name));
        mCtlArtistDetail.setExpandedTitleColor(getResources().getColor(R.color.white));

        mSongLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRcvSongOfArtist.setLayoutManager(mSongLayoutManager);
        mRcvSongOfArtist.setItemAnimator(new DefaultItemAnimator());
        mRcvSongOfArtist.setAdapter(mSongAdapter);

        mSongAdapter.setOnItemClickListener(new SongAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(View view, int position) {
                mPresenter.onIntentPrepared(position);
            }
        });

        // Load current audio
        onLoadAudio();

        if (getIntent() != null) {
            if (getIntent().getStringExtra("ARTIST_ID") != null) {
                mPresenter.getArtistInfo(this, getIntent().getStringExtra("ARTIST_ID"));
            }
        }

        mIvSongControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onActionAudio();
            }
        });

        mLayoutPlayBottom.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                songControlLayoutAction();
            }
        });

        mSbDuration.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                onSeekTo(seekBar.getProgress() * 1000);
            }
        });
    }

    @Override
    public void setArtistName(String name) {
        mCtlArtistDetail.setTitle(name);
    }

    @Override
    public void updatePlayList(List<Song> songList) {
        mSongAdapter.addItems(songList);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void startIntentPlay(List<Song> audioList, int audioIndex) {

        mStorage.storeAudio(audioList);
        mStorage.storeAudioIndex(audioIndex);
        startActivity(PlayActivity.getStartIntent(this)
                .putExtra(AppConstants.PLAY_TYPE, PlaybackStatus.PLAYING));
    }

    // Declare broadcast receiver
    private BroadcastReceiver mMediaChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateAudioInformation();
        }
    };

    private BroadcastReceiver mMediaDurationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int duration = intent.getIntExtra("duration", 0);
            mSbDuration.setMax(duration);
        }
    };

    private BroadcastReceiver mMediaCurrentPositionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int current = intent.getIntExtra("currentPosition", 0);
            mSbDuration.setProgress(current);
        }
    };

    private BroadcastReceiver mPlaybackStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            PlaybackStatus status = (PlaybackStatus) intent.getSerializableExtra("playback_status");
            setActionButton(status);
        }
    };

    // Register broadcast receiver
    private void register_getPlaybackStatus() {
        LocalBroadcastManager.getInstance(ArtistDetailActivity.this).registerReceiver(
                mPlaybackStatusReceiver, new IntentFilter(AppConstants.Broadcast_ACTION_AUDIO));
    }

    private void register_getMediaChange() {
        LocalBroadcastManager.getInstance(ArtistDetailActivity.this).registerReceiver(
                mMediaChangeReceiver, new IntentFilter(AppConstants.Broadcast_MEDIA_CHANGE));
    }

    private void register_getMediaDuration() {
        LocalBroadcastManager.getInstance(ArtistDetailActivity.this).registerReceiver(
                mMediaDurationReceiver, new IntentFilter(AppConstants.Broadcast_SEND_DURATION));
    }

    private void register_getMediaCurrentPosition() {
        LocalBroadcastManager.getInstance(ArtistDetailActivity.this).registerReceiver(
                mMediaCurrentPositionReceiver, new IntentFilter(AppConstants.Broadcast_SEND_CURRENT));
    }

    // Send broadcast receiver
    private void onLoadAudio() {
        Log.e(TAG, "loadBroadcast");
        sendBroadcast(new Intent(AppConstants.Broadcast_LOAD_AUDIO));
    }

    private void onActionAudio() {
        Log.e(TAG, "actionBroadcast");
        sendBroadcast(new Intent(AppConstants.Broadcast_ACTION_AUDIO));
    }

    private void onSeekTo(int position) {
        Log.e(TAG, "seekToBroadcast");
        Intent broadcastIntent = new Intent(AppConstants.Broadcast_SEEK_TO);
        broadcastIntent.putExtra("seekTo", position);
        sendBroadcast(broadcastIntent);
    }

    // Method support
    private void updateAudioInformation() {
        Song currentSong = mStorage.loadAudio().get(mStorage.loadAudioIndex());
        mTvSongName.setText(currentSong.getName());
        mTvArtitst.setText(currentSong.getArtist());
    }

    private void setActionButton(PlaybackStatus status) {
        mIvSongControl.setImageResource(status == PlaybackStatus.PLAYING
                ? R.drawable.ic_pause_pink_24dp : R.drawable.ic_play_arrow_pink_24dp);

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void songControlLayoutAction() {
        startActivity(PlayActivity.getStartIntent(this)
                .putExtra(AppConstants.PLAY_TYPE, PlaybackStatus.RESUMING));
    }

}
