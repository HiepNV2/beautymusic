package com.example.beautymusic.ui.main.albums.detail;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import com.example.beautymusic.R;
import com.example.beautymusic.data.db.model.Album;
import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.ui.base.BasePresenter;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class AlbumDetailPresenter<V extends AlbumDetailMvpView,
        I extends AlbumDetailMvpInteractor> extends BasePresenter<V, I>
        implements AlbumDetailMvpPresenter<V, I> {

    private static final String TAG = "AlbumDetailPresenter";

    @Inject
    public AlbumDetailPresenter(I mvpInteractor) {
        super(mvpInteractor);
        mSongList = new ArrayList<>();
    }

    private List<Song> mSongList;

    @Override
    public void getAlbumInfo(Context context, String albumId) {
        Log.d(TAG, "albumId: " + albumId);
        Uri uri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;

        String[] projection = {
                MediaStore.Audio.Albums._ID,
                MediaStore.Audio.Albums.ALBUM,
                MediaStore.Audio.Albums.ARTIST,
                MediaStore.Audio.Albums.ALBUM_ART
        };

        String selection = "_id = " + albumId;

        Cursor albumCursor = context.getContentResolver().query(uri,
                projection,
                selection,
                null,
                null);

        String albumArt = null;
        String albumName = null;

        if (albumCursor != null) {
            while (albumCursor.moveToNext()) {
                albumName = albumCursor.getString(1);
                albumArt = albumCursor.getString(3);
            }
            albumCursor.close();

            getMvpView().setAlbumName(albumName);
            loadBitmapFromFile(context, albumArt);

            getSongsPerAlbum(context, albumId);
        }

    }

    @Override
    public void getSongsPerAlbum(Context context, String albumId) {
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DURATION,
        };

        String selection = "is_music != 0 and album_id = " + albumId;

        Cursor songCursor = context.getContentResolver().query(uri,
                projection,
                selection,
                null,
                null);

        if (songCursor != null) {
            while (songCursor.moveToNext()) {
                if (songCursor.getInt(5) > 30000) {
                    Song song = new Song();

                    String id = songCursor.getString(0);
                    String name = songCursor.getString(1);
                    String albumName = songCursor.getString(2);
                    String artist = songCursor.getString(3);
                    String path = songCursor.getString(4);

                    song.setId(id);
                    song.setName(name);
                    song.setArtist(artist);
                    song.setAlbum(albumName);
                    song.setPath(path);

                    Log.d("Name :" + name, " Album :" + albumName);
                    Log.d("Path :" + path, " Artist :" + artist);

                    mSongList.add(song);
                }
            }
            songCursor.close();

            Log.d(TAG, "mSongList: " + mSongList.size());
            getMvpView().updatePlayList(mSongList);
        }
    }

    private void loadBitmapFromFile(Context context, String path) {
        Bitmap bitmap;

        if (path == null || path.isEmpty()) {
            bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.bg_album);
        } else {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            bitmap = BitmapFactory.decodeFile(path, options);
        }

        getMvpView().setAlbumArtCollapsing(bitmap);
    }

    @Override
    public void onIntentPrepared(int position) {
        getMvpView().startIntentPlay(mSongList, position);
    }
}
