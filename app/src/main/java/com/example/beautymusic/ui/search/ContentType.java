package com.example.beautymusic.ui.search;

public enum ContentType {
    SONG,
    ALBUM,
    ARTIST
}
