package com.example.beautymusic.ui.main.songs;

import android.content.Context;

import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.ui.base.MvpInteractor;
import com.example.beautymusic.ui.base.MvpPresenter;
import com.example.beautymusic.ui.base.MvpView;

import java.util.List;

public interface SongMvpPresenter<V extends MvpView, I extends MvpInteractor>
        extends MvpPresenter<V, I> {

    void getAllAudioFromDevice(Context context);

    void onLoadData(List<Song> songList);

    void onPlayIntentPrepared(int audioIndex);

}
