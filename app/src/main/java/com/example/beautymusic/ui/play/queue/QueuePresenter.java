package com.example.beautymusic.ui.play.queue;

import android.os.Bundle;

import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.ui.base.BasePresenter;

import java.util.List;

import javax.inject.Inject;

public class QueuePresenter<V extends QueueMvpView, I extends QueueMvpInteractor>
        extends BasePresenter<V, I> implements QueueMvpPresenter<V, I> {

    private List<Song> mSongList;

    @Inject
    public QueuePresenter(I mvpInteractor) {
        super(mvpInteractor);
    }

    @Override
    public void onViewPrepared(List<Song> audioList) {
        mSongList = audioList;
        getMvpView().updatePlayList(mSongList);
    }

}
