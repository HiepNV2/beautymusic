package com.example.beautymusic.ui.play;

import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beautymusic.R;
import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.service.MediaPlayerService;
import com.example.beautymusic.ui.base.BaseActivity;
import com.example.beautymusic.ui.main.songs.SongAdapter;
import com.example.beautymusic.ui.play.queue.QueueActivity;
import com.example.beautymusic.ui.search.SearchableActivity;
import com.example.beautymusic.utils.AppConstants;
import com.example.beautymusic.utils.CircularSeekBar;
import com.example.beautymusic.utils.PlaybackStatus;
import com.example.beautymusic.utils.StorageUtil;

import java.util.List;

import javax.inject.Inject;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class PlayActivity extends BaseActivity implements PlayMvpView, View.OnClickListener,
        CircularSeekBar.OnCircularSeekBarChangeListener {

    private static final String TAG = "PlayActivity";
    private static final int REQUEST_CODE_PLAYLIST = 1;
    private static final Object ONGOING_NOTIFICATION_ID = 1;

    @Inject
    PlayMvpPresenter<PlayMvpView, PlayMvpInteractor> mPresenter;

    @Inject
    SongAdapter mSongAdapter;

    @Inject
    LinearLayoutManager mSongLayoutManager;

    @Inject
    StorageUtil mStorage;

    private boolean isPlaying = false;
    private boolean isStartIntent = false;

    private MediaPlayerService mMediaService;

    private Toolbar mToolbar;
    private RecyclerView mRcvSong;
    private CircularSeekBar mSbSongDuration;
    private TextView mTvSongDuration;
    private FloatingActionButton mBtnAction;
    private ImageButton mIbnPrevios, mIbnShuffle, mIbnRepeat, mIbnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        setContentView(R.layout.activity_play);

        getActivityComponent().inject(this);

        mPresenter.onAttach(this);

        startMediaService();

        bindView();

        setUp();

        register_getMediaChange();

        register_getMediaDuration();

        register_getMediaCurrentPosition();

        register_getPlaybackStatus();

        register_getAlert();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        // load audio list
        if (mStorage.loadAudio() != null) {
            mPresenter.onAudioListPrepared(mStorage.loadAudio());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");

        if (!isPlaying && !isStartIntent) {
            stopPlayForeground();
        }

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMediaChangeReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMediaDurationReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMediaCurrentPositionReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mPlaybackStatusReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mGetAlertReceiver);

        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_play, menu);

        MenuItem searchItem = menu.findItem(R.id.mnu_search);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(
                new ComponentName(this, SearchableActivity.class)));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                isStartIntent = true;
                finish();
            case R.id.mnu_search:
                onSearchRequested();
                return true;
            case R.id.mnu_sort_list:
                mPresenter.onIntentPlayingQueuePrepared();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Toast.makeText(this, "Searching by: " + query, Toast.LENGTH_SHORT).show();

        } else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            String uri = intent.getDataString();
            Toast.makeText(this, "Suggestion: " + uri, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibn_previos:
                onPreviousAudio();
                break;
            case R.id.ibn_shuffle:
                onShuffleChanged();
                break;
            case R.id.ibn_repeat:
                onRepeatChanged();
                break;
            case R.id.ibn_next:
                onNextAudio();
                break;
            case R.id.fab_play:
                onActionAudio();
                break;
        }
    }

    @Override
    protected void bindView() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar_play);
        mRcvSong = (RecyclerView) findViewById(R.id.rv_songs);
        mIbnPrevios = (ImageButton) findViewById(R.id.ibn_previos);
        mIbnShuffle = (ImageButton) findViewById(R.id.ibn_shuffle);
        mIbnRepeat = (ImageButton) findViewById(R.id.ibn_repeat);
        mIbnNext = (ImageButton) findViewById(R.id.ibn_next);
        mBtnAction = (FloatingActionButton) findViewById(R.id.fab_play);
        mTvSongDuration = (TextView) findViewById(R.id.tv_song_duration);
        mSbSongDuration = (CircularSeekBar) findViewById(R.id.csb_song_duration);
    }

    @Override
    protected void setUp() {

        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mSongLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRcvSong.setLayoutManager(mSongLayoutManager);
        mRcvSong.setItemAnimator(new DefaultItemAnimator());
        mRcvSong.setAdapter(mSongAdapter);

        // load state play
        loadStateShuffle();
        loadStateRepeat();

        // check playback status
        if (getIntent().getSerializableExtra(AppConstants.PLAY_TYPE) != null) {
            PlaybackStatus status =
                    (PlaybackStatus) getIntent().getSerializableExtra(AppConstants.PLAY_TYPE);
            if (status == PlaybackStatus.PLAYING) {
                playMedia();
            } else {
                resumeMedia();
            }
        }

        // listener
        mSongAdapter.setOnItemClickListener(new SongAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(View view, int position) {
                mStorage.storeAudioIndex(position);
                mSongAdapter.notifyDataSetChanged();
                playMedia();
            }
        });

        mIbnPrevios.setOnClickListener(this);
        mIbnShuffle.setOnClickListener(this);
        mIbnRepeat.setOnClickListener(this);
        mIbnNext.setOnClickListener(this);
        mBtnAction.setOnClickListener(this);
        mSbSongDuration.setOnSeekBarChangeListener(this);
    }

    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    // Declare broadcast receiver
    private BroadcastReceiver mMediaChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateAudioInformation();
        }
    };

    private BroadcastReceiver mMediaDurationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int duration = intent.getIntExtra("duration", 0);
            mSbSongDuration.setMax(duration);
        }
    };

    private BroadcastReceiver mMediaCurrentPositionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int current = intent.getIntExtra("currentPosition", 0);
            mSbSongDuration.setProgress(current);
            setTimer(current);
        }
    };

    private BroadcastReceiver mPlaybackStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            PlaybackStatus status = (PlaybackStatus) intent.getSerializableExtra("playback_status");
            setActionButton(status);
            isPlaying = status.equals(PlaybackStatus.PLAYING);
        }
    };

    private BroadcastReceiver mGetAlertReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra("alert");
            showAlert(message);
        }
    };

    // Register broadcast receiver
    private void register_getMediaChange() {
        LocalBroadcastManager.getInstance(PlayActivity.this).registerReceiver(
                mMediaChangeReceiver, new IntentFilter(AppConstants.Broadcast_MEDIA_CHANGE));
    }

    private void register_getMediaDuration() {
        LocalBroadcastManager.getInstance(PlayActivity.this).registerReceiver(
                mMediaDurationReceiver, new IntentFilter(AppConstants.Broadcast_SEND_DURATION));
    }

    private void register_getMediaCurrentPosition() {
        LocalBroadcastManager.getInstance(PlayActivity.this).registerReceiver(
                mMediaCurrentPositionReceiver, new IntentFilter(AppConstants.Broadcast_SEND_CURRENT));
    }

    private void register_getPlaybackStatus() {
        LocalBroadcastManager.getInstance(PlayActivity.this).registerReceiver(
                mPlaybackStatusReceiver, new IntentFilter(AppConstants.Broadcast_ACTION_AUDIO));
    }

    private void register_getAlert() {
        LocalBroadcastManager.getInstance(PlayActivity.this).registerReceiver(
                mGetAlertReceiver, new IntentFilter(AppConstants.Broadcast_SHOW_ALERT));
    }

    @Override
    public void setPlayList(List<Song> songList) {
        mSongAdapter.addItems(songList);
    }

    @Override
    public void playMedia() {
        onPlayNewAudio();
    }

    @Override
    public void resumeMedia() {
        onLoadAudio();
    }

    @Override
    public void startIntentPlayingQueue() {
        startActivity(QueueActivity.getStartIntent(this));
    }

    private void showAlert(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProgressChanged(CircularSeekBar circularSeekBar, int progress, boolean fromUser) {
        if (fromUser) {
            setTimer(progress);
        }
    }

    @Override
    public void onStopTrackingTouch(CircularSeekBar seekBar) {
        onSeekTo(seekBar.getProgress() * 1000);
    }

    @Override
    public void onStartTrackingTouch(CircularSeekBar seekBar) {

    }

    // Send broadcast receiver
    private void onLoadAudio() {
        Log.e(TAG, "loadBroadcast");
        sendBroadcast(new Intent(AppConstants.Broadcast_LOAD_AUDIO));
    }

    private void onActionAudio() {
        Log.e(TAG, "actionBroadcast");
        sendBroadcast(new Intent(AppConstants.Broadcast_ACTION_AUDIO));
    }

    private void onPlayNewAudio() {
        Log.e(TAG, "playNewBroadcast");
        sendBroadcast(new Intent(AppConstants.Broadcast_PLAY_NEW_AUDIO));
    }

    private void onPreviousAudio() {
        Log.e(TAG, "previousBroadcast");
        sendBroadcast(new Intent(AppConstants.Broadcast_PREVIOS_AUDIO));
    }

    private void onNextAudio() {
        Log.e(TAG, "nextBroadcast");
        sendBroadcast(new Intent(AppConstants.Broadcast_NEXT_AUDIO));
    }

    private void onSeekTo(int position) {
        Log.e(TAG, "seekToBroadcast");
        Intent broadcastIntent = new Intent(AppConstants.Broadcast_SEEK_TO);
        broadcastIntent.putExtra("seekTo", position);
        sendBroadcast(broadcastIntent);
    }

    private void stopPlayForeground() {
        Log.e(TAG, "stopForegroundBroadcast");
        sendBroadcast(new Intent(AppConstants.Broadcast_PLAY_FOREGROUND));
    }

    // Start service connection
    private void startMediaService() {
        startService(new Intent(getBaseContext(), MediaPlayerService.class));
    }

    // Method support
    public static Intent getStartIntent(Context context) {
        return new Intent(context, PlayActivity.class);
    }

    private void setTimer(int currentPosition) {
        String time = "";
        if (currentPosition < 60) {
            time = currentPosition < 10 ? "00:0" + currentPosition : "00:" + currentPosition;
        } else {
            int minute = (int) currentPosition / 60;
            int second = (int) currentPosition % 60;

            time = minute < 10 ? "0" + minute + ":" +
                    (second < 10 ? "0" + second : second) : minute + ":" +
                    (second < 10 ? "0" + second : second);
        }
        mTvSongDuration.setText(time);
    }

    private void updateAudioInformation() {
        // Set toolbar
        String song = mStorage.loadAudio().get(mStorage.loadAudioIndex()).getName();
        String artist = mStorage.loadAudio().get(mStorage.loadAudioIndex()).getArtist();
        mToolbar.setTitle(song);
        mToolbar.setSubtitle(artist);
    }

    private void setActionButton(PlaybackStatus status) {
        mBtnAction.setImageResource(status == PlaybackStatus.PLAYING
                ? R.drawable.ic_pause_white_24dp : R.drawable.ic_play_arrow_white_24dp);
    }

    private void loadStateShuffle() {
        mIbnShuffle.setImageResource(AppConstants.isShuffle ? R.drawable.ic_shuffle_white_24dp
                : R.drawable.ic_shuffle_gray_24dp);
    }

    private void loadStateRepeat() {
        mIbnRepeat.setImageResource(AppConstants.isRepeat ? R.drawable.ic_repeat_white_24dp
                : R.drawable.ic_repeat_gray_24dp);
    }

    private void onShuffleChanged() {
        AppConstants.isShuffle = !AppConstants.isShuffle;
        mIbnShuffle.setImageResource(AppConstants.isShuffle ? R.drawable.ic_shuffle_white_24dp
                : R.drawable.ic_shuffle_gray_24dp);
    }

    private void onRepeatChanged() {
        AppConstants.isRepeat = !AppConstants.isRepeat;
        mIbnRepeat.setImageResource(AppConstants.isRepeat ? R.drawable.ic_repeat_white_24dp
                : R.drawable.ic_repeat_gray_24dp);
    }
}
