package com.example.beautymusic.ui.main.albums;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.telephony.mbms.MbmsErrors;
import android.util.Log;

import com.example.beautymusic.data.db.model.Album;
import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.ui.base.BasePresenter;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class AlbumPresenter<V extends AlbumMvpView,
        I extends AlbumMvpInteractor> extends BasePresenter<V, I>
        implements AlbumMvpPresenter<V, I> {

    private static final String TAG = "AlbumPresenter";
    private List<Album> mAlbumList;

    @Inject
    public AlbumPresenter(I mvpInteractor) {
        super(mvpInteractor);
        mAlbumList = new ArrayList<>();
    }

    @Override
    public void onViewPrepared() {

    }

    @Override
    public void getAllAlbumFromDevice(Context context) {
        Uri uri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;

        String[] projection = new String[]{
                MediaStore.Audio.Albums._ID,
                MediaStore.Audio.Albums.ALBUM,
                MediaStore.Audio.Albums.ARTIST,
                MediaStore.Audio.Albums.ALBUM_ART
        };

        String selection = null;
        String[] selectionArgs = null;
        String sortOrder = MediaStore.Audio.Media.ALBUM + " ASC";

        Cursor albumCursor = context.getContentResolver().query(uri,
                projection,
                selection,
                selectionArgs,
                sortOrder
        );

        if (albumCursor != null) {
            while (albumCursor.moveToNext()) {

                Album album = new Album();
                String id = albumCursor.getString(0);
                String name = albumCursor.getString(1);
                String artist = albumCursor.getString(2);
                String art = albumCursor.getString(3);

                album.setAlbumId(id);
                album.setAlbumName(name);
                album.setAlbumArtist(artist);
                album.setAlbumArt(art);

                Log.d(TAG, "AlbumArt: "+album.getAlbumArt());

                mAlbumList.add(album);
            }
            albumCursor.close();

            Log.d(TAG, "mAlbumList: " + mAlbumList.size());
            getMvpView().updateAlbum(mAlbumList);
        }
    }

    @Override
    public void onIntentPrepared(int position) {
        getMvpView().sendIntentAlbumDetail(mAlbumList.get(position));
    }
}
