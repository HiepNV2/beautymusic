package com.example.beautymusic.ui.play.queue;

import android.os.Bundle;

import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.ui.base.MvpPresenter;

import java.util.List;

public interface QueueMvpPresenter<V extends QueueMvpView, I extends QueueMvpInteractor>
        extends MvpPresenter<V, I> {

    void onViewPrepared(List<Song> audioList);
}
