package com.example.beautymusic.ui.main;

import android.os.Bundle;

import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.ui.base.BasePresenter;
import com.example.beautymusic.ui.base.MvpInteractor;
import com.example.beautymusic.ui.base.MvpView;

import java.util.List;

import javax.inject.Inject;

public class MainPresenter<V extends MainMvpView, I extends MainMvpInteractor>
        extends BasePresenter<V, I> implements MainMvpPresenter<V, I> {

    @Inject
    public MainPresenter(I mvpInteractor) {
        super(mvpInteractor);
    }

}
