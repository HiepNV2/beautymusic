package com.example.beautymusic.ui.main.artists;

import android.content.Context;

import com.example.beautymusic.ui.base.MvpInteractor;
import com.example.beautymusic.ui.base.MvpPresenter;
import com.example.beautymusic.ui.base.MvpView;

public interface ArtistMvpPresenter<V extends MvpView, I extends MvpInteractor>
        extends MvpPresenter<V, I> {

    void getAllArtistFromDevice(Context context);

    void onIntentPrepared(int position);
}
