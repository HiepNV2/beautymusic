package com.example.beautymusic.ui.main;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beautymusic.R;
import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.service.MediaPlayerService;
import com.example.beautymusic.ui.base.BaseActivity;
import com.example.beautymusic.ui.main.albums.detail.AlbumDetailActivity;
import com.example.beautymusic.ui.main.artists.detail.ArtistDetailActivity;
import com.example.beautymusic.ui.play.PlayActivity;
import com.example.beautymusic.ui.search.SearchableActivity;
import com.example.beautymusic.utils.AppConstants;
import com.example.beautymusic.utils.PlaybackStatus;
import com.example.beautymusic.utils.StorageUtil;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class MainActivity extends BaseActivity implements MainMvpView {

    private static final String TAG = "MainActivity";

    @Inject
    MainMvpPresenter<MainMvpView, MainMvpInteractor> mPresenter;

    @Inject
    MainPagerAdapter mPagerAdapter;

    @Inject
    StorageUtil mStorage;

    private Toolbar mToolbarMain;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private TextView mTvSongName, mTvArtitst;
    private AppCompatSeekBar mSbDuration;
    private ImageView mIvSongPicture, mIvSongControl;
    private LinearLayout mLayoutPlayBottom;
    private SearchView mSearchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getActivityComponent().inject(this);

        mPresenter.onAttach(this);

        bindView();

        setUp();

        register_getMediaChange();

        register_getMediaDuration();

        register_getMediaCurrentPosition();

        register_getPlaybackStatus();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        onLoadAudio();
    }

    @Override
    protected void onPause() {
        mIvSongPicture.clearAnimation();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        mPresenter.onDetach();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMediaChangeReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMediaDurationReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMediaCurrentPositionReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mPlaybackStatusReceiver);

        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem searchItem = menu.findItem(R.id.mnu_search);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mSearchView = (SearchView) searchItem.getActionView();
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(
                new ComponentName(this, SearchableActivity.class)));
        mSearchView.setIconifiedByDefault(true);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnu_search:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Toast.makeText(this, "Searching by: " + query, Toast.LENGTH_SHORT).show();

        } else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            String uri = intent.getDataString();
            Toast.makeText(this, "Suggestion: " + uri, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void bindView() {
        mToolbarMain = (Toolbar) findViewById(R.id.toolbar_main);
        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mViewPager = (ViewPager) findViewById(R.id.main_view_pager);
        mTvSongName = (TextView) findViewById(R.id.tv_song_name);
        mTvArtitst = (TextView) findViewById(R.id.tv_song_artist);
        mSbDuration = (AppCompatSeekBar) findViewById(R.id.sb_song_duration);
        mIvSongControl = (ImageView) findViewById(R.id.iv_song_control);
        mIvSongPicture = (ImageView) findViewById(R.id.iv_song_picture);
        mLayoutPlayBottom = (LinearLayout) findViewById(R.id.ll_song_control);
    }

    @Override
    protected void setUp() {

        setSupportActionBar(mToolbarMain);

        mPagerAdapter = new MainPagerAdapter(getSupportFragmentManager());

        mPagerAdapter.setCount(3);

        mViewPager.setAdapter(mPagerAdapter);

        mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.tab_song)));
        mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.tab_albums)));
        mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.tab_artist)));

        mViewPager.setOffscreenPageLimit(mTabLayout.getTabCount());

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));

        mTabLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        mIvSongControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onActionAudio();
            }
        });

        mLayoutPlayBottom.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                songControlLayoutAction();
            }
        });

        mSbDuration.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                onSeekTo(seekBar.getProgress() * 1000);
            }
        });
    }

    // Declare broadcast receiver
    private BroadcastReceiver mMediaChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateAudioInformation();
        }
    };

    private BroadcastReceiver mMediaDurationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int duration = intent.getIntExtra("duration", 0);
            mSbDuration.setMax(duration);
        }
    };

    private BroadcastReceiver mMediaCurrentPositionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int current = intent.getIntExtra("currentPosition", 0);
            mSbDuration.setProgress(current);
        }
    };

    private BroadcastReceiver mPlaybackStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            PlaybackStatus status = (PlaybackStatus) intent.getSerializableExtra("playback_status");
            setActionButton(status);
        }
    };

    // Register broadcast receiver
    private void register_getPlaybackStatus() {
        LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(
                mPlaybackStatusReceiver, new IntentFilter(AppConstants.Broadcast_ACTION_AUDIO));
    }

    private void register_getMediaChange() {
        LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(
                mMediaChangeReceiver, new IntentFilter(AppConstants.Broadcast_MEDIA_CHANGE));
    }

    private void register_getMediaDuration() {
        LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(
                mMediaDurationReceiver, new IntentFilter(AppConstants.Broadcast_SEND_DURATION));
    }

    private void register_getMediaCurrentPosition() {
        LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(
                mMediaCurrentPositionReceiver, new IntentFilter(AppConstants.Broadcast_SEND_CURRENT));
    }

    // Send broadcast receiver
    private void onLoadAudio() {
        Log.e(TAG, "loadBroadcast");
        sendBroadcast(new Intent(AppConstants.Broadcast_LOAD_AUDIO));
    }

    private void onActionAudio() {
        Log.e(TAG, "actionBroadcast");
        sendBroadcast(new Intent(AppConstants.Broadcast_ACTION_AUDIO));

    }

    private void onSeekTo(int position) {
        Log.e(TAG, "seekToBroadcast");
        Intent broadcastIntent = new Intent(AppConstants.Broadcast_SEEK_TO);
        broadcastIntent.putExtra("seekTo", position);
        sendBroadcast(broadcastIntent);
    }

    // Method support
    public static Intent getStartIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    private void updateAudioInformation() {
        Song currentSong = mStorage.loadAudio().get(mStorage.loadAudioIndex());
        mTvSongName.setText(currentSong.getName());
        mTvArtitst.setText(currentSong.getArtist());
    }

    private void setActionButton(PlaybackStatus status) {
        mIvSongControl.setImageResource(status == PlaybackStatus.PLAYING
                ? R.drawable.ic_pause_pink_24dp : R.drawable.ic_play_arrow_pink_24dp);

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void songControlLayoutAction() {
        startActivity(PlayActivity.getStartIntent(this)
                .putExtra(AppConstants.PLAY_TYPE, PlaybackStatus.RESUMING));
    }

}
