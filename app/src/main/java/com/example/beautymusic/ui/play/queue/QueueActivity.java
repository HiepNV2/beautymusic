package com.example.beautymusic.ui.play.queue;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.beautymusic.R;
import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.ui.base.BaseActivity;
import com.example.beautymusic.ui.main.songs.SongAdapter;
import com.example.beautymusic.ui.play.PlayActivity;
import com.example.beautymusic.utils.AppConstants;
import com.example.beautymusic.utils.ItemMoveCallback;
import com.example.beautymusic.utils.PlaybackStatus;
import com.example.beautymusic.utils.StorageUtil;

import java.util.List;

import javax.inject.Inject;

public class QueueActivity extends BaseActivity implements QueueMvpView {

    private static final String TAG = "QueueActivity";

    @Inject
    QueueMvpPresenter<QueueMvpView, QueueMvpInteractor> mPresenter;

    @Inject
    SongAdapter mSongAdapter;

    @Inject
    LinearLayoutManager mSongLayoutManager;

    @Inject
    StorageUtil mStorage;

    private Toolbar mToolbarQueue;
    private RecyclerView mRcvSong;
    private ImageView mIvSongControl, mIvSongPicture;
    private TextView mTvSongName, mTvArtitst;
    private AppCompatSeekBar mSbDuration;
    private LinearLayout mLayoutPlayBottom;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, QueueActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_queue);

        getActivityComponent().inject(this);

        mPresenter.onAttach(this);

        bindView();

        setUp();

        register_getMediaChange();

        register_getMediaDuration();

        register_getMediaCurrentPosition();

        register_getPlaybackStatus();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMediaChangeReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMediaDurationReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMediaCurrentPositionReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mPlaybackStatusReceiver);

        super.onDestroy();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startPlayIntent();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBackPressed() {
        startPlayIntent();
        super.onBackPressed();
    }

    @Override
    protected void bindView() {
        mToolbarQueue = (Toolbar) findViewById(R.id.toolbar_queue);
        mRcvSong = (RecyclerView) findViewById(R.id.rv_playing_queue);
        mIvSongControl = (ImageView) findViewById(R.id.iv_song_control);
        mIvSongPicture = (ImageView) findViewById(R.id.iv_song_picture);
        mTvSongName = (TextView) findViewById(R.id.tv_song_name);
        mTvArtitst = (TextView) findViewById(R.id.tv_song_artist);
        mSbDuration = (AppCompatSeekBar) findViewById(R.id.sb_song_duration);
        mLayoutPlayBottom = (LinearLayout) findViewById(R.id.ll_song_control);
    }

    @Override
    protected void setUp() {

        setSupportActionBar(mToolbarQueue);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mSongLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRcvSong.setLayoutManager(mSongLayoutManager);
        mRcvSong.setItemAnimator(new DefaultItemAnimator());

        ItemTouchHelper.Callback callback =
                new ItemMoveCallback((ItemMoveCallback.ItemTouchHelperContract) mSongAdapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(mRcvSong);

        mRcvSong.setAdapter(mSongAdapter);

        // Load audio from preference
        mPresenter.onViewPrepared(mStorage.loadAudio());
        // Load current audio
        onLoadAudio();

        // Listener
        mIvSongControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onActionAudio();
            }
        });

        mLayoutPlayBottom.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                songControlLayoutAction();
            }
        });

        mSbDuration.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                onSeekTo(seekBar.getProgress() * 1000);
            }
        });
    }

    @Override
    public void updatePlayList(List<Song> songList) {
        mSongAdapter.setSortVisibility(true);
        mSongAdapter.addItems(songList);
    }

    // Declare broadcast receiver
    private BroadcastReceiver mMediaChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateAudioInformation();
        }
    };

    private BroadcastReceiver mMediaDurationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int duration = intent.getIntExtra("duration", 0);
            mSbDuration.setMax(duration);
        }
    };

    private BroadcastReceiver mMediaCurrentPositionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int current = intent.getIntExtra("currentPosition", 0);
            mSbDuration.setProgress(current);
        }
    };

    private BroadcastReceiver mPlaybackStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            PlaybackStatus status = (PlaybackStatus) intent.getSerializableExtra("playback_status");
            setActionButton(status);
        }
    };

    // Register broadcast receiver
    private void register_getPlaybackStatus() {
        LocalBroadcastManager.getInstance(QueueActivity.this).registerReceiver(
                mPlaybackStatusReceiver, new IntentFilter(AppConstants.Broadcast_ACTION_AUDIO));
    }

    private void register_getMediaChange() {
        LocalBroadcastManager.getInstance(QueueActivity.this).registerReceiver(
                mMediaChangeReceiver, new IntentFilter(AppConstants.Broadcast_MEDIA_CHANGE));
    }

    private void register_getMediaDuration() {
        LocalBroadcastManager.getInstance(QueueActivity.this).registerReceiver(
                mMediaDurationReceiver, new IntentFilter(AppConstants.Broadcast_SEND_DURATION));
    }

    private void register_getMediaCurrentPosition() {
        LocalBroadcastManager.getInstance(QueueActivity.this).registerReceiver(
                mMediaCurrentPositionReceiver, new IntentFilter(AppConstants.Broadcast_SEND_CURRENT));
    }

    // Send broadcast receiver
    private void onLoadAudio() {
        Log.e(TAG, "loadBroadcast");
        sendBroadcast(new Intent(AppConstants.Broadcast_LOAD_AUDIO));
    }

    private void onActionAudio() {
        Log.e(TAG, "actionBroadcast");
        sendBroadcast(new Intent(AppConstants.Broadcast_ACTION_AUDIO));
    }

    private void onSeekTo(int position) {
        Log.e(TAG, "seekToBroadcast");
        Intent broadcastIntent = new Intent(AppConstants.Broadcast_SEEK_TO);
        broadcastIntent.putExtra("seekTo", position);
        sendBroadcast(broadcastIntent);
    }

    // Method support
    private void updateAudioInformation() {
        Song currentSong = mStorage.loadAudio().get(mStorage.loadAudioIndex());
        mTvSongName.setText(currentSong.getName());
        mTvArtitst.setText(currentSong.getArtist());
    }

    private void setActionButton(PlaybackStatus status) {
        mIvSongControl.setImageResource(status == PlaybackStatus.PLAYING
                ? R.drawable.ic_pause_pink_24dp : R.drawable.ic_play_arrow_pink_24dp);

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void songControlLayoutAction() {
        startPlayIntent();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void startPlayIntent() {
        finish();
    }
}
