package com.example.beautymusic.ui.play;

import android.media.MediaPlayer;

import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.ui.base.MvpView;

import java.util.List;

public interface PlayMvpView extends MvpView {

    void setPlayList(List<Song> songList);

    void playMedia();

    void resumeMedia();

    void startIntentPlayingQueue();

}
