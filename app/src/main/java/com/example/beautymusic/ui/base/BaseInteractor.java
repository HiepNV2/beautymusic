package com.example.beautymusic.ui.base;

import javax.inject.Inject;

public class BaseInteractor implements MvpInteractor {

    @Inject
    protected BaseInteractor() {

    }
}
