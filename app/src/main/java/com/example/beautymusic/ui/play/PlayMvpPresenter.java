package com.example.beautymusic.ui.play;

import android.os.Bundle;

import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.ui.base.MvpPresenter;

import java.util.List;

public interface PlayMvpPresenter<V extends PlayMvpView, I extends PlayMvpInteractor>
        extends MvpPresenter<V, I> {

    void onAudioListPrepared(List<Song> audioList);

    void onIntentPlayingQueuePrepared();

}
