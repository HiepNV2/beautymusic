package com.example.beautymusic.ui.main.songs;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.ui.base.BasePresenter;
import com.example.beautymusic.utils.StorageUtil;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class SongPresenter<V extends SongMvpView,
        I extends SongMvpInteractor> extends BasePresenter<V, I>
        implements SongMvpPresenter<V, I> {

    private static final String TAG = "SongPresenter";
    private List<Song> mSongList;

    @Inject
    public SongPresenter(I mvpInteractor) {
        super(mvpInteractor);
        mSongList = new ArrayList<>();
    }

    @Override
    public void getAllAudioFromDevice(final Context context) {

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DURATION,
        };

        Cursor songCursor = context.getContentResolver().query(uri,
                projection,
                null,
                null,
                null);

        if (songCursor != null) {
            while (songCursor.moveToNext()) {

                // check duration > 30s
                if (songCursor.getInt(5) > 30000) {
                    Song song = new Song();

                    String id = songCursor.getString(0);
                    String name = songCursor.getString(1);
                    String album = songCursor.getString(2);
                    String artist = songCursor.getString(3);
                    String path = songCursor.getString(4);

                    song.setId(id);
                    song.setName(name);
                    song.setAlbum(album);
                    song.setArtist(artist);
                    song.setPath(path);

                    Log.d("Name :" + name, " Album :" + album);
                    Log.d("Path :" + path, " Artist :" + artist);

                    mSongList.add(song);
                }
            }
            songCursor.close();

            Log.d(TAG, "mSongList: " + mSongList.size());
            getMvpView().updateSong(mSongList);
        }
    }

    @Override
    public void onLoadData(List<Song> songList) {
        mSongList = songList;
        getMvpView().updateSong(mSongList);
    }

    @Override
    public void onPlayIntentPrepared(int audioIndex) {
        getMvpView().startIntentPlay(mSongList, audioIndex);
    }

}
