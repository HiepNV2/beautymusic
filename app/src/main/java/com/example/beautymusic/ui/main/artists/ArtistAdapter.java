package com.example.beautymusic.ui.main.artists;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.beautymusic.R;
import com.example.beautymusic.data.db.model.Album;
import com.example.beautymusic.data.db.model.Artist;
import com.example.beautymusic.ui.base.BaseViewHolder;
import com.example.beautymusic.ui.main.albums.AlbumAdapter;
import com.example.beautymusic.ui.main.songs.SongAdapter;
import com.example.beautymusic.utils.AppConstants;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

public class ArtistAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private Context mContext;
    private List<Artist> mArtistList;
    private int mViewType;
    private static OnItemClickListener mOnItemClickLister;

    public interface OnItemClickListener {
        void onItemClicked(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickLister = listener;
    }

    public ArtistAdapter(Context context, List<Artist> mArtistList) {
        this.mArtistList = mArtistList;
        this.mContext = context;
        mViewType = AppConstants.VIEW_TYPE_GRID;
    }

    private int getViewType() {
        return mViewType;
    }

    public void setViewType(int mViewType) {
        this.mViewType = mViewType;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        switch (getViewType()) {
            case AppConstants.VIEW_TYPE_LIST:
                return new ViewHolder(
                        LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_artist_list_view, viewGroup, false));
            case AppConstants.VIEW_TYPE_GRID:
            default:
                return new ViewHolder(
                        LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_artist_grid_view, viewGroup, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mViewType == AppConstants.VIEW_TYPE_LIST) {
            return AppConstants.VIEW_TYPE_LIST;
        } else {
            return AppConstants.VIEW_TYPE_GRID;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder baseViewHolder, int i) {
        baseViewHolder.onBind(i);
    }

    @Override
    public int getItemCount() {
        return mArtistList.size();
    }

    public void addItems(List<Artist> artistList) {
        mArtistList.addAll(artistList);
        notifyDataSetChanged();
    }

    public class ViewHolder extends BaseViewHolder {

        ImageView artistPiture;
        TextView artistName, numberAlbum, numberSong;

        private ViewHolder(View itemView) {
            super(itemView);

            artistPiture = (ImageView) itemView.findViewById(R.id.iv_item_artist_picture);
            artistName = (TextView) itemView.findViewById(R.id.tv_item_artist_name);
            numberAlbum = (TextView) itemView.findViewById(R.id.tv_item_number_album);
            numberSong = (TextView) itemView.findViewById(R.id.tv_item_number_song);
        }

        @Override
        protected void clear() {
            artistPiture.setImageDrawable(null);
            artistName.setText("");
            numberAlbum.setText("");
            numberSong.setText("");
        }

        public void onBind(final int position) {

            final Artist artist = mArtistList.get(position);

            if (artist.getArtistArt() != null){
                try {

                    InputStream inputStream = mContext.getContentResolver()
                            .openInputStream(Uri.parse(artist.getArtistArt()));
                    Bitmap art = BitmapFactory.decodeStream(inputStream);
                    artistPiture.setImageBitmap(art);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }

            if (artist.getArtistName() != null) {
                artistName.setText(artist.getArtistName());
            }

            if (artist.getNumberAlbums() > 0) {
                numberAlbum.setText(artist.getNumberAlbums() + " albums");
            }

            if (artist.getNumberSongs() > 0) {
                numberSong.setText(artist.getNumberSongs() + " songs");
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickLister.onItemClicked(v, position);
                }
            });
        }
    }
}
