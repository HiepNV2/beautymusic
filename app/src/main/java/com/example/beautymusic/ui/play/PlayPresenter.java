package com.example.beautymusic.ui.play;

import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.ui.base.BasePresenter;

import java.util.List;

import javax.inject.Inject;

public class PlayPresenter<V extends PlayMvpView, I extends PlayMvpInteractor>
        extends BasePresenter<V, I> implements PlayMvpPresenter<V, I> {

    private static final String TAG = "PlayPresenter";

    @Inject
    public PlayPresenter(I mvpInteractor) {
        super(mvpInteractor);
    }

    @Override
    public void onAudioListPrepared(List<Song> audioList) {
        getMvpView().setPlayList(audioList);
    }

    @Override
    public void onIntentPlayingQueuePrepared() {
        getMvpView().startIntentPlayingQueue();
    }
}
