package com.example.beautymusic.ui.main.artists.detail;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import com.example.beautymusic.data.db.model.Artist;
import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.ui.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ArtistDetailPresenter<V extends ArtistDetailMvpView,
        I extends ArtistDetailMvpInteractor> extends BasePresenter<V, I>
        implements ArtistDetailMvpPresenter<V, I> {

    private static final String TAG = "ArtistDetailPresenter";

    @Inject
    public ArtistDetailPresenter(I mvpInteractor) {
        super(mvpInteractor);
        mSongList = new ArrayList<>();
    }

    private List<Song> mSongList;

    @Override
    public void getArtistInfo(Context context, String artistId) {
        Log.d(TAG, "artistId: " + artistId);
        Uri uri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;

        String[] projection = {
                MediaStore.Audio.Artists._ID,
                MediaStore.Audio.Artists.ARTIST
        };

        String selection = "_id = " + artistId;

        Cursor artistCursor = context.getContentResolver().query(uri,
                projection,
                selection,
                null,
                null);

        String artistName = null;

        if (artistCursor != null) {
            while (artistCursor.moveToNext()) {
                artistName = artistCursor.getString(1);
            }
            artistCursor.close();

            getMvpView().setArtistName(artistName);

            getSongsPerArtist(context, artistId);
        }
    }

    @Override
    public void getSongsPerArtist(Context context, String artistId) {

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DURATION,
        };

        String selection = "artist_id = " + artistId;

        Cursor songCursor = context.getContentResolver().query(uri,
                projection,
                selection,
                null,
                null);

        if (songCursor != null) {
            while (songCursor.moveToNext()) {

                if (songCursor.getInt(5) > 30000) {
                    Song song = new Song();

                    String id = songCursor.getString(0);
                    String songName = songCursor.getString(1);
                    String albumName = songCursor.getString(2);
                    String artistName = songCursor.getString(3);
                    String path = songCursor.getString(4);

                    song.setId(id);
                    song.setName(songName);
                    song.setAlbum(albumName);
                    song.setArtist(artistName);
                    song.setPath(path);

                    Log.d("Name :" + songName, " Album :" + albumName);
                    Log.d("Path :" + path, " Artist :" + artistName);

                    mSongList.add(song);
                }

            }
            songCursor.close();

            Log.d(TAG, "mSongList: " + mSongList.size());
            getMvpView().updatePlayList(mSongList);
        }
    }

    @Override
    public void onIntentPrepared(int position) {
        getMvpView().startIntentPlay(mSongList, position);
    }

}
