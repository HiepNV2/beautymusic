package com.example.beautymusic.ui.main.songs;

import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.ui.base.MvpView;

import java.util.List;

public interface SongMvpView extends MvpView {

    void updateSong(List<Song> songList);

    void startIntentPlay(List<Song> audioList, int audioIndex);

}
