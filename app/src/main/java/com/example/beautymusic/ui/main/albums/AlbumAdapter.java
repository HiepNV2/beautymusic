package com.example.beautymusic.ui.main.albums;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beautymusic.R;
import com.example.beautymusic.data.db.model.Album;
import com.example.beautymusic.ui.base.BaseViewHolder;
import com.example.beautymusic.ui.main.albums.detail.AlbumDetailActivity;
import com.example.beautymusic.utils.AppConstants;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.EventListener;
import java.util.List;

import javax.inject.Inject;

public class AlbumAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static OnItemClickListener mOnItemClickLister;
    private int mViewType;
    private List<Album> mAlbumList;

    public AlbumAdapter(Context context, List<Album> mAlbumList) {
        this.mAlbumList = mAlbumList;
        mViewType = AppConstants.VIEW_TYPE_GRID;
    }

    public interface OnItemClickListener {
        void onItemClicked(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickLister = listener;
    }

    private int getViewType() {
        return mViewType;
    }

    public void setViewType(int mViewType) {
        this.mViewType = mViewType;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        switch (getViewType()) {
            case AppConstants.VIEW_TYPE_LIST:
                return new ViewHolder(
                        LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_album_list_view, viewGroup, false));
            case AppConstants.VIEW_TYPE_GRID:
            default:
                return new ViewHolder(
                        LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_album_grid_view, viewGroup, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mViewType == AppConstants.VIEW_TYPE_LIST) {
            return AppConstants.VIEW_TYPE_LIST;
        } else {
            return AppConstants.VIEW_TYPE_GRID;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder baseViewHolder, int i) {
        baseViewHolder.onBind(i);
    }

    @Override
    public int getItemCount() {
        return mAlbumList.size();
    }

    public void addItems(List<Album> albumList) {
        mAlbumList.addAll(albumList);
        notifyDataSetChanged();
    }

    public class ViewHolder extends BaseViewHolder {

        ImageView albumPiture;
        TextView albumName, albumArtist;

        private ViewHolder(View itemView) {
            super(itemView);

            albumPiture = (ImageView) itemView.findViewById(R.id.iv_item_album_picture);
            albumName = (TextView) itemView.findViewById(R.id.tv_item_album_name);
            albumArtist = (TextView) itemView.findViewById(R.id.tv_item_album_artist);
        }

        @Override
        protected void clear() {
            albumPiture.setImageDrawable(null);
            albumName.setText("");
            albumArtist.setText("");
        }

        public void onBind(final int position) {

            final Album album = mAlbumList.get(position);

            if (album.getAlbumArt() != null) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bitmap = BitmapFactory.decodeFile(album.getAlbumArt(), options);

                albumPiture.setImageBitmap(bitmap);
            }

            if (album.getAlbumName() != null) {
                albumName.setText(album.getAlbumName());
            }

            if (album.getAlbumArtist() != null) {
                albumArtist.setText(album.getAlbumArtist());
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickLister.onItemClicked(v, position);
                }
            });

        }
    }
}
