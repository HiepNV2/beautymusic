package com.example.beautymusic.ui.main.albums.detail;

import android.content.Context;

import com.example.beautymusic.data.db.model.Album;
import com.example.beautymusic.ui.base.MvpInteractor;
import com.example.beautymusic.ui.base.MvpPresenter;
import com.example.beautymusic.ui.base.MvpView;
import com.example.beautymusic.ui.main.albums.AlbumMvpInteractor;

public interface AlbumDetailMvpPresenter<V extends MvpView, I extends MvpInteractor>
        extends MvpPresenter<V, I> {

    void getAlbumInfo(Context context, String albumId);

    void getSongsPerAlbum(Context context, String albumId);

    void onIntentPrepared(int position);
}
