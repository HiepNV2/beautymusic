package com.example.beautymusic.ui.main.songs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.beautymusic.R;
import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.ui.base.BaseViewHolder;
import com.example.beautymusic.utils.AppConstants;
import com.example.beautymusic.utils.ItemMoveCallback;
import com.example.beautymusic.utils.StorageUtil;

import java.util.Collections;
import java.util.List;

public class SongAdapter extends RecyclerView.Adapter<BaseViewHolder> implements ItemMoveCallback.ItemTouchHelperContract {

    private static final String TAG = "SongAdapter";

    private boolean isSort = false;

    private static OnItemClickListener mOnItemClickLister;

    private List<Song> mSongList;

    private int mViewType;

    private Context mContext;

    public SongAdapter(List<Song> songList, Context context) {
        this.mSongList = songList;
        this.mContext = context;
        this.mViewType = AppConstants.VIEW_TYPE_LIST;
    }

    public int getViewType() {
        return mViewType;
    }

    public void setViewType(int mViewType) {
        this.mViewType = mViewType;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        switch (getViewType()) {
            case AppConstants.VIEW_TYPE_GRID:
                return new ViewHolder(
                        LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_song_grid_view, viewGroup, false));
            case AppConstants.VIEW_TYPE_LIST:
            default:
                return new ViewHolder(
                        LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_song_list_view, viewGroup, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mViewType == AppConstants.VIEW_TYPE_LIST) {
            return AppConstants.VIEW_TYPE_LIST;
        } else {
            return AppConstants.VIEW_TYPE_GRID;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder baseViewHolder, int i) {
        baseViewHolder.onBind(i);
    }

    @Override
    public int getItemCount() {
        return mSongList.size();
    }

    public void addItems(List<Song> songList) {
        if (mSongList != null && mSongList.size() > 0) {
            mSongList.clear();
        }
        mSongList.addAll(songList);
        notifyDataSetChanged();
    }

    public void clearData() {
        if (mSongList != null) {
            mSongList.clear();
            notifyDataSetChanged();
        }
    }

    @Override
    public void onRowMoved(int fromPosition, int toPosition) {
        StorageUtil storage = new StorageUtil(mContext);
        Song activeAudio = mSongList.get(storage.loadAudioIndex());

        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(mSongList, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(mSongList, i, i - 1);
            }
        }

        notifyItemMoved(fromPosition, toPosition);

        for (Song song : mSongList) {
            if (song.getId().equals(activeAudio.getId())) {
                storage.storeAudioIndex(mSongList.indexOf(song));
            }
        }
        storage.storeAudio(mSongList);
    }

    @Override
    public void onRowSelected(ViewHolder myViewHolder) {
        myViewHolder.songSort.setImageResource(R.drawable.ic_swap_vert_black_24dp);
    }

    @Override
    public void onRowClear(ViewHolder myViewHolder) {
        myViewHolder.songSort.setImageResource(R.drawable.ic_queue_music_black_24dp);
    }

    public interface OnItemClickListener {
        void onItemClicked(View view, int position);
    }


    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickLister = listener;
    }

    public List<Song> getAudioList() {
        return mSongList;
    }

    public void setSortVisibility(boolean b) {
        isSort = b;
    }

    public class ViewHolder extends BaseViewHolder {

        ImageButton songSort;
        ImageView songPiture;
        TextView songName, songArtist;

        private ViewHolder(View itemView) {
            super(itemView);

            songSort = (ImageButton) itemView.findViewById(R.id.ibn_move);
            songPiture = (ImageView) itemView.findViewById(R.id.iv_item_song_picture);
            songName = (TextView) itemView.findViewById(R.id.tv_item_song_name);
            songArtist = (TextView) itemView.findViewById(R.id.tv_item_song_artist);
        }

        @Override
        protected void clear() {
            songPiture.setImageDrawable(null);
            songName.setText("");
            songArtist.setText("");
        }

        public void onBind(final int position) {
            super.onBind(position);

            final Song song = mSongList.get(position);

            songSort.setVisibility(isSort ? View.VISIBLE : View.GONE);

            songPiture.setImageResource(R.drawable.ic_beat);

            if (song.getName() != null) {
                songName.setText(song.getName());
            }

            if (song.getArtist() != null) {
                songArtist.setText(song.getArtist());
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickLister.onItemClicked(v, position);
                }
            });

        }
    }
}
