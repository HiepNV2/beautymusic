package com.example.beautymusic.ui.main.albums;

import android.content.Context;

import com.example.beautymusic.ui.base.MvpInteractor;
import com.example.beautymusic.ui.base.MvpPresenter;
import com.example.beautymusic.ui.base.MvpView;

public interface AlbumMvpPresenter<V extends MvpView, I extends MvpInteractor>
        extends MvpPresenter<V, I> {

    void onViewPrepared();

    void getAllAlbumFromDevice(Context context);

    void onIntentPrepared(int position);
}
