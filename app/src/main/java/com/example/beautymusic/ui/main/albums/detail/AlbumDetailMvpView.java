package com.example.beautymusic.ui.main.albums.detail;

import android.graphics.Bitmap;

import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.ui.base.MvpView;

import java.util.List;

public interface AlbumDetailMvpView extends MvpView {

    void setAlbumArtCollapsing(Bitmap bitmap);

    void setAlbumName(String name);

    void updatePlayList(List<Song> songList);

    void startIntentPlay(List<Song> audioList, int audioIndex);

}
