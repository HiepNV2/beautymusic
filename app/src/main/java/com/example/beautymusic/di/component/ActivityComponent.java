package com.example.beautymusic.di.component;

import com.example.beautymusic.di.PerActivity;
import com.example.beautymusic.di.module.ActivityModule;
import com.example.beautymusic.ui.main.MainActivity;
import com.example.beautymusic.ui.main.albums.AlbumFragment;
import com.example.beautymusic.ui.main.albums.detail.AlbumDetailActivity;
import com.example.beautymusic.ui.main.artists.ArtistFragment;
import com.example.beautymusic.ui.main.artists.detail.ArtistDetailActivity;
import com.example.beautymusic.ui.main.songs.SongFragment;
import com.example.beautymusic.ui.play.PlayActivity;
import com.example.beautymusic.ui.play.queue.QueueActivity;
import com.example.beautymusic.ui.search.SearchableActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity activity);

    void inject(PlayActivity activity);

    void inject(QueueActivity activity);

    void inject(SongFragment fragment);

    void inject(AlbumFragment fragment);

    void inject(AlbumDetailActivity activity);

    void inject(ArtistFragment fragment);

    void inject(ArtistDetailActivity activity);

    void inject(SearchableActivity activity);

}
