package com.example.beautymusic.di.component;

import com.example.beautymusic.di.PerService;
import com.example.beautymusic.di.module.ServiceModule;
import com.example.beautymusic.ui.play.PlayActivity;

import javax.inject.Singleton;

import dagger.Component;

@PerService
@Component(dependencies = ApplicationComponent.class, modules = ServiceModule.class)
public interface ServiceComponent {

}
