package com.example.beautymusic.di.module;

import android.app.Service;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.example.beautymusic.di.ActivityContext;
import com.example.beautymusic.service.MediaPlayerService;

import dagger.Module;
import dagger.Provides;

@Module
public class ServiceModule {

    private final Service mService;

    public ServiceModule(Service service) {
        mService = service;
    }
}
