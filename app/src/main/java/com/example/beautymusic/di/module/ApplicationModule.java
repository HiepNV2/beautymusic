package com.example.beautymusic.di.module;

import android.app.Application;
import android.content.Context;

import com.example.beautymusic.di.ApplicationContext;
import com.example.beautymusic.di.DatabaseInfo;
import com.example.beautymusic.utils.AppConstants;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @DatabaseInfo
    String provideDatabaseName() {
        return AppConstants.DB_NAME;
    }
}
