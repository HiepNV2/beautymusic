package com.example.beautymusic.di.component;

import android.app.Application;
import android.content.Context;

import com.example.beautymusic.BeautyMusic;
import com.example.beautymusic.di.ApplicationContext;
import com.example.beautymusic.di.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(BeautyMusic app);

    @ApplicationContext
    Context context();

    Application application();
}
