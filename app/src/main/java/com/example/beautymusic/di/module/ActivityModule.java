package com.example.beautymusic.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;

import com.example.beautymusic.data.db.model.Album;
import com.example.beautymusic.data.db.model.Artist;
import com.example.beautymusic.data.db.model.Song;
import com.example.beautymusic.di.ActivityContext;
import com.example.beautymusic.di.PerActivity;
import com.example.beautymusic.ui.main.MainInteractor;
import com.example.beautymusic.ui.main.MainMvpInteractor;
import com.example.beautymusic.ui.main.MainMvpPresenter;
import com.example.beautymusic.ui.main.MainMvpView;
import com.example.beautymusic.ui.main.MainPagerAdapter;
import com.example.beautymusic.ui.main.MainPresenter;
import com.example.beautymusic.ui.main.albums.AlbumAdapter;
import com.example.beautymusic.ui.main.albums.AlbumInteractor;
import com.example.beautymusic.ui.main.albums.AlbumMvpInteractor;
import com.example.beautymusic.ui.main.albums.AlbumMvpPresenter;
import com.example.beautymusic.ui.main.albums.AlbumMvpView;
import com.example.beautymusic.ui.main.albums.AlbumPresenter;
import com.example.beautymusic.ui.main.albums.detail.AlbumDetailInteractor;
import com.example.beautymusic.ui.main.albums.detail.AlbumDetailMvpInteractor;
import com.example.beautymusic.ui.main.albums.detail.AlbumDetailMvpPresenter;
import com.example.beautymusic.ui.main.albums.detail.AlbumDetailMvpView;
import com.example.beautymusic.ui.main.albums.detail.AlbumDetailPresenter;
import com.example.beautymusic.ui.main.artists.ArtistAdapter;
import com.example.beautymusic.ui.main.artists.ArtistInteractor;
import com.example.beautymusic.ui.main.artists.ArtistMvpInteractor;
import com.example.beautymusic.ui.main.artists.ArtistMvpPresenter;
import com.example.beautymusic.ui.main.artists.ArtistMvpView;
import com.example.beautymusic.ui.main.artists.ArtistPresenter;
import com.example.beautymusic.ui.main.artists.detail.ArtistDetailInteractor;
import com.example.beautymusic.ui.main.artists.detail.ArtistDetailMvpInteractor;
import com.example.beautymusic.ui.main.artists.detail.ArtistDetailMvpPresenter;
import com.example.beautymusic.ui.main.artists.detail.ArtistDetailMvpView;
import com.example.beautymusic.ui.main.artists.detail.ArtistDetailPresenter;
import com.example.beautymusic.ui.main.songs.SongAdapter;
import com.example.beautymusic.ui.main.songs.SongInteractor;
import com.example.beautymusic.ui.main.songs.SongMvpInteractor;
import com.example.beautymusic.ui.main.songs.SongMvpPresenter;
import com.example.beautymusic.ui.main.songs.SongMvpView;
import com.example.beautymusic.ui.main.songs.SongPresenter;
import com.example.beautymusic.ui.play.PlayInteractor;
import com.example.beautymusic.ui.play.PlayMvpInteractor;
import com.example.beautymusic.ui.play.PlayMvpPresenter;
import com.example.beautymusic.ui.play.PlayMvpView;
import com.example.beautymusic.ui.play.PlayPresenter;
import com.example.beautymusic.ui.play.queue.QueueInteractor;
import com.example.beautymusic.ui.play.queue.QueueMvpInteractor;
import com.example.beautymusic.ui.play.queue.QueueMvpPresenter;
import com.example.beautymusic.ui.play.queue.QueueMvpView;
import com.example.beautymusic.ui.play.queue.QueuePresenter;
import com.example.beautymusic.utils.AppConstants;
import com.example.beautymusic.utils.StorageUtil;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    @PerActivity
    MainMvpPresenter<MainMvpView, MainMvpInteractor> provideMainPresenter(
            MainPresenter<MainMvpView, MainMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    AlbumMvpPresenter<AlbumMvpView, AlbumMvpInteractor> provideAlbumPresenter(
            AlbumPresenter<AlbumMvpView, AlbumMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    AlbumDetailMvpPresenter<AlbumDetailMvpView, AlbumDetailMvpInteractor> provideAlbumDetailPresenter(
            AlbumDetailPresenter<AlbumDetailMvpView, AlbumDetailMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    ArtistMvpPresenter<ArtistMvpView, ArtistMvpInteractor> provideArtistPresenter(
            ArtistPresenter<ArtistMvpView, ArtistMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    ArtistDetailMvpPresenter<ArtistDetailMvpView, ArtistDetailMvpInteractor> provideArtistDetailPresenter(
            ArtistDetailPresenter<ArtistDetailMvpView, ArtistDetailMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    SongMvpPresenter<SongMvpView, SongMvpInteractor> provideSongPresenter(
            SongPresenter<SongMvpView, SongMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    PlayMvpPresenter<PlayMvpView, PlayMvpInteractor> providePlayPresenter(
            PlayPresenter<PlayMvpView, PlayMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    QueueMvpPresenter<QueueMvpView, QueueMvpInteractor> provideQueuePresenter(
            QueuePresenter<QueueMvpView, QueueMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MainMvpInteractor provideMainMvpInteractor(MainInteractor interactor) {
        return interactor;
    }

    @Provides
    @PerActivity
    PlayMvpInteractor providePlayMvpInteractor(PlayInteractor interactor) {
        return interactor;
    }

    @Provides
    @PerActivity
    QueueMvpInteractor provideQueueMvpInteractor(QueueInteractor interactor) {
        return interactor;
    }

    @Provides
    @PerActivity
    AlbumMvpInteractor provideAlbumMvpInteractor(AlbumInteractor interactor) {
        return interactor;
    }

    @Provides
    @PerActivity
    AlbumDetailMvpInteractor provideAlbumDetailMvpInteractor(AlbumDetailInteractor interactor) {
        return interactor;
    }

    @Provides
    @PerActivity
    ArtistDetailMvpInteractor provideArtistDetailMvpInteractor(ArtistDetailInteractor interactor) {
        return interactor;
    }

    @Provides
    @PerActivity
    ArtistMvpInteractor provideArtistMvpInteractor(ArtistInteractor interactor) {
        return interactor;
    }

    @Provides
    @PerActivity
    SongMvpInteractor provideSongMvpInteractor(SongInteractor interactor) {
        return interactor;
    }

    @Provides
    MainPagerAdapter provideMainPagerAdapter(AppCompatActivity activity) {
        return new MainPagerAdapter(activity.getSupportFragmentManager());
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new LinearLayoutManager(activity);
    }

    @Provides
    GridLayoutManager provideGridLayoutManager(AppCompatActivity activity) {
        return new GridLayoutManager(activity, 2);
    }

    @Provides
    SongAdapter provideSongAdapter() {
        return new SongAdapter(new ArrayList<Song>(), provideContext());
    }

    @Provides
    AlbumAdapter provideAlbumAdapter() {
        return new AlbumAdapter(provideContext(), new ArrayList<Album>());
    }

    @Provides
    ArtistAdapter provideArtistAdapter() {
        return new ArtistAdapter(provideContext(), new ArrayList<Artist>());
    }

    @Provides
    StorageUtil provideStorageUtil(){
        return new StorageUtil(provideContext());
    }

}
