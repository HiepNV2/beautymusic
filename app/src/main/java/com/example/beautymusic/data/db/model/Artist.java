package com.example.beautymusic.data.db.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Artist implements Serializable {
    private String artistId;
    private String artistName;
    private String artistArt;
    private int numberAlbums;
    private int numberSongs;

    public Artist() {

    }

    public Artist(String artistId, String artistName, String artistArt, int numberAlbums, int numberSongs) {
        this.artistId = artistId;
        this.artistName = artistName;
        this.artistArt = artistArt;
        this.numberAlbums = numberAlbums;
        this.numberSongs = numberSongs;
    }

    public String getArtistId() {
        return artistId;
    }

    public void setArtistId(String artistId) {
        this.artistId = artistId;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getArtistArt() {
        return artistArt;
    }

    public void setArtistArt(String artistArt) {
        this.artistArt = artistArt;
    }

    public int getNumberAlbums() {
        return numberAlbums;
    }

    public void setNumberAlbums(int numberAlbums) {
        this.numberAlbums = numberAlbums;
    }

    public int getNumberSongs() {
        return numberSongs;
    }

    public void setNumberSongs(int numberSongs) {
        this.numberSongs = numberSongs;
    }
}
