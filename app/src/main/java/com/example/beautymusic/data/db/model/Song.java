package com.example.beautymusic.data.db.model;

import java.io.Serializable;

public class Song implements Serializable {
    private String id;
    private String album;
    private String artist;
    private String name;
    private String path;

    public Song() {

    }

    public Song(String id, String album, String artist, String name, String path) {
        this.id = id;
        this.album = album;
        this.artist = artist;
        this.name = name;
        this.path = path;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
